---
title: "Generator losowych danych"
subtitle: "Projekt na ZPOiF"
author: "Marcin Łukaszyk i Michał Wdowski"
output: pdf_document
---

##Opis projektu
Celem projektu jest stworzenie aplikacji w języku Java, która generuje tabele z losowo wygenerowanymi danymi osobowymi Polaków. Dane mają bazować na statystykach z różnych źródeł zewnętrznych, a część z nich będzie można dopasowywać według uznania użytkownika.

##Cel projektu
Celem projektu ma być praktyczne wykorzystanie umiejętności zdobytych na kursie z Zaawanswoanego Programowania Obiektowego i Funkcyjnego, zwłaszcza w zakresie analizy danych. Ponadto aplikacja umożliwi generowanie danych przydatnych np. do testowania.

##Fukcjonalności
Docelowa aplikacja ma posiadać graficzny interfejs użytkownika, na którym znajdą się kontrolki do:

* Wyświetlania pomocy w obsłudze programu
* Wyświetlania informacji o aplikacji i twórcach
* Wyboru kolumn w danych
* Ustawiania właściwości kolumn takich jak:
    * Rozkład cech
    * Poziom braków w danych
* Ustalenia liczby rekordów w docelowym zbiorze danych
* Wyboru formatu zbioru z danymi

##Zawartość
Lista możliwych kolumn wygląda następująco:

* płeć biologiczna (ang. *sex*) i płeć społeczna (ang. *gender*)
* imiona
* nazwisko i nazwisko rodowe
* miejsce i data urodzenia, numer PESEl
* miejscowość, gmina, powiat, województwo
* numer, data wydania i data ważności dowodu osobistego
* numer telefonu
* adres e-mail
* wzrost i waga
* kolor oczu i włosów
* stan cywilny
* wyznanie
* imię ojca i matki
* wykształcenie i stan zatrudnienia
* orientacja seksualna

Nie wykluczamy ewentualnego rozwoju aplikacji o nowe funkcjonalności