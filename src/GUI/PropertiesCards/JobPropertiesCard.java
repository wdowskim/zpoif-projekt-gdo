package GUI.PropertiesCards;

import Base.Global;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class JobPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private Double defaultMiddleNameFraction = Global.DEFAULT_PROPERTIES.getJobPropeties().probJob;

    public JobPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Frakcja osób zatrudnionych");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(defaultMiddleNameFraction.toString());
        this.centerPanel.add(this.textField1);

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(defaultMiddleNameFraction.toString());
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newMiddleNameFraction = Double.parseDouble(this.textField1.getText());
                if (newMiddleNameFraction < 0 || newMiddleNameFraction > 1)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji.\nPodaj liczbę rzeczywistą od 0 do 1.");
                    return;
                }
                Global.CURRENT_PROPERTIES.getJobPropeties().probJob = newMiddleNameFraction;
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji.\nPodaj liczbę rzeczywistą od 0 do 1.");
            }
        }
    }
}
