package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class HairPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private JLabel description4;
    private JTextField textField4;

    private JLabel description5;
    private JTextField textField5;

    private JCheckBox checkBox;

    private Double probAuburnDefault = Global.DEFAULT_PROPERTIES.getHairProperties().probAuburn;
    private Double probBlackDefault = Global.DEFAULT_PROPERTIES.getHairProperties().probBlack;
    private Double probBlondDefault = Global.DEFAULT_PROPERTIES.getHairProperties().probBlond;
    private Double probBrownDefault = Global.DEFAULT_PROPERTIES.getHairProperties().probBrown;
    private Double probRedDefault = Global.DEFAULT_PROPERTIES.getHairProperties().probRed;
    private Boolean allowGrayDefault = Global.DEFAULT_PROPERTIES.getHairProperties().allowGray;

    public HairPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Stosunek włosów kasztanowych do całości");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(probAuburnDefault.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Stosunek włosów czarnych do całości");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(probBlackDefault.toString());
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Stosunek włosów blond do całości");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(probBlondDefault.toString());
        this.centerPanel.add(this.textField3);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 4
        this.description4 = new JLabel("Stosunek włosów brązowych do całości");
        this.centerPanel.add(description4);
        this.textField4 = new JTextField(probBrownDefault.toString());
        this.centerPanel.add(this.textField4);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 5
        this.description5 = new JLabel("Stosunek włosów rudych do całości");
        this.centerPanel.add(description5);
        this.textField5 = new JTextField(probRedDefault.toString());
        this.centerPanel.add(this.textField5);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 6
        this.checkBox = new JCheckBox();
        this.checkBox.setText("Czy pozwlić na siwe włosy?");
        this.checkBox.setSelected(this.allowGrayDefault);
        this.centerPanel.add(this.checkBox);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(probAuburnDefault.toString());
            this.textField2.setText(probBlackDefault.toString());
            this.textField3.setText(probBlondDefault.toString());
            this.textField4.setText(probBrownDefault.toString());
            this.textField5.setText(probRedDefault.toString());
            this.checkBox.setSelected(allowGrayDefault);
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newAuburnProb = Double.parseDouble(this.textField1.getText());
                double newBlackProb = Double.parseDouble(this.textField2.getText());
                double newBlondProb = Double.parseDouble(this.textField3.getText());
                double newBrownProb = Double.parseDouble(this.textField4.getText());
                double newRedProb = Double.parseDouble(this.textField5.getText());

                if (newAuburnProb < 0 || newAuburnProb > 1000000 ||
                        newBlackProb < 0 || newBlackProb > 1000000 ||
                        newBlondProb < 0 || newBlondProb > 1000000 ||
                        newBrownProb < 0 || newBrownProb > 1000000 ||
                        newRedProb < 0 || newRedProb > 1000000)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
                    return;
                }

                Global.CURRENT_PROPERTIES.getHairProperties().probAuburn = newAuburnProb;
                Global.CURRENT_PROPERTIES.getHairProperties().probBlack = newBlackProb;
                Global.CURRENT_PROPERTIES.getHairProperties().probBlond = newBlondProb;
                Global.CURRENT_PROPERTIES.getHairProperties().probBrown = newBrownProb;
                Global.CURRENT_PROPERTIES.getHairProperties().probRed = newRedProb;
                Global.CURRENT_PROPERTIES.getHairProperties().allowGray = this.checkBox.isSelected();
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
            }
        }
    }
}
