package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class BirthMonthPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private JLabel description4;
    private JTextField textField4;

    private JLabel description5;
    private JTextField textField5;

    private JLabel description6;
    private JTextField textField6;

    private JLabel description7;
    private JTextField textField7;

    private JLabel description8;
    private JTextField textField8;

    private JLabel description9;
    private JTextField textField9;

    private JLabel description10;
    private JTextField textField10;

    private JLabel description11;
    private JTextField textField11;

    private JLabel description12;
    private JTextField textField12;

    private String defaultJanuarySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().januarySymbol;
    private String defaultFebruarySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().februarySymbol;
    private String defaultMarchSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().marchSymbol;
    private String defaultAprilSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().aprilSymbol;
    private String defaultMaySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().maySymbol;
    private String defaultJuneSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().juneSymbol;
    private String defaultJulySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().julySymbol;
    private String defaultAugustSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().augustSymbol;
    private String defaultSeptemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().septemberSymbol;
    private String defaultOctoberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().octoberSymbol;
    private String defaultNovemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().novemberSymbol;
    private String defaultDecemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().decemberSymbol;

    public BirthMonthPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Oznaczenie stycznia");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(defaultJanuarySymbol.toString());
        this.centerPanel.add(this.textField1);

        // 2
        this.description2 = new JLabel("Oznaczenie lutego");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(defaultFebruarySymbol.toString());
        this.centerPanel.add(this.textField2);

        // 3
        this.description3 = new JLabel("Oznaczenie marca");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(defaultMarchSymbol.toString());
        this.centerPanel.add(this.textField3);

        // 4
        this.description4 = new JLabel("Oznaczenie kwietnia");
        this.centerPanel.add(description4);
        this.textField4 = new JTextField(defaultAprilSymbol.toString());
        this.centerPanel.add(this.textField4);

        // 5
        this.description5 = new JLabel("Oznaczenie maja");
        this.centerPanel.add(description5);
        this.textField5 = new JTextField(defaultMaySymbol.toString());
        this.centerPanel.add(this.textField5);

        // 6
        this.description6 = new JLabel("Oznaczenie czerwca");
        this.centerPanel.add(description6);
        this.textField6 = new JTextField(defaultJuneSymbol.toString());
        this.centerPanel.add(this.textField6);

        // 7
        this.description7 = new JLabel("Oznaczenie lipca");
        this.centerPanel.add(description7);
        this.textField7 = new JTextField(defaultJulySymbol.toString());
        this.centerPanel.add(this.textField7);

        // 8
        this.description8 = new JLabel("Oznaczenie sierpnia");
        this.centerPanel.add(description8);
        this.textField8 = new JTextField(defaultAugustSymbol.toString());
        this.centerPanel.add(this.textField8);

        // 9
        this.description9 = new JLabel("Oznaczenie września");
        this.centerPanel.add(description9);
        this.textField9 = new JTextField(defaultSeptemberSymbol.toString());
        this.centerPanel.add(this.textField9);

        // 10
        this.description10 = new JLabel("Oznaczenie października");
        this.centerPanel.add(description10);
        this.textField10 = new JTextField(defaultOctoberSymbol.toString());
        this.centerPanel.add(this.textField10);

        // 11
        this.description11 = new JLabel("Oznaczenie listopada");
        this.centerPanel.add(description11);
        this.textField11 = new JTextField(defaultNovemberSymbol.toString());
        this.centerPanel.add(this.textField11);

        // 12
        this.description12 = new JLabel("Oznaczenie stycznia");
        this.centerPanel.add(description12);
        this.textField12 = new JTextField(defaultDecemberSymbol.toString());
        this.centerPanel.add(this.textField12);

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(defaultJanuarySymbol);
            this.textField2.setText(defaultFebruarySymbol);
            this.textField3.setText(defaultMarchSymbol);
            this.textField4.setText(defaultAprilSymbol);
            this.textField5.setText(defaultMaySymbol);
            this.textField6.setText(defaultJuneSymbol);
            this.textField7.setText(defaultJulySymbol);
            this.textField8.setText(defaultAugustSymbol);
            this.textField9.setText(defaultSeptemberSymbol);
            this.textField10.setText(defaultOctoberSymbol);
            this.textField11.setText(defaultNovemberSymbol);
            this.textField12.setText(defaultDecemberSymbol);
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().januarySymbol = this.textField1.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().februarySymbol = this.textField2.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().marchSymbol = this.textField3.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().aprilSymbol = this.textField4.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().maySymbol = this.textField5.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().juneSymbol = this.textField6.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().julySymbol = this.textField7.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().augustSymbol = this.textField8.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().septemberSymbol = this.textField9.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().octoberSymbol = this.textField10.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().novemberSymbol = this.textField11.getText();
            Global.CURRENT_PROPERTIES.getBirthMonthProperties().decemberSymbol = this.textField12.getText();
        }
    }
}
