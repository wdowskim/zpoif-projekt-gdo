package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class EducationPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private JLabel description4;
    private JTextField textField4;

    private JLabel description5;
    private JTextField textField5;

    private JLabel description6;
    private JTextField textField6;

    private JLabel description7;
    private JTextField textField7;

    private Double probPodst = Global.DEFAULT_PROPERTIES.getEducationProperties().probPodst;
    private Double probGimn = Global.DEFAULT_PROPERTIES.getEducationProperties().probGimn;
    private Double probZasa = Global.DEFAULT_PROPERTIES.getEducationProperties().probZasa;
    private Double probZaBr = Global.DEFAULT_PROPERTIES.getEducationProperties().probZaBr;
    private Double probSrBr = Global.DEFAULT_PROPERTIES.getEducationProperties().probSrBr;
    private Double probsred = Global.DEFAULT_PROPERTIES.getEducationProperties().probsred;
    private Double probWyzs = Global.DEFAULT_PROPERTIES.getEducationProperties().probWyzs;

    public EducationPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(22, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Stosunek wykształcenia podstawowego do całości");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(probPodst.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Stosunek wykształcenia gimnazjalnego do całości");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(probGimn.toString());
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Stosunek wykształcenia zasadniczego do całości");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(probZasa.toString());
        this.centerPanel.add(this.textField3);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 4
        this.description4 = new JLabel("Stosunek wykształcenia zasadniczego-branżowego do całości");
        this.centerPanel.add(description4);
        this.textField4 = new JTextField(probZaBr.toString());
        this.centerPanel.add(this.textField4);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 5
        this.description5 = new JLabel("Stosunek wykształcenia średniego-branżowego do całości");
        this.centerPanel.add(description5);
        this.textField5 = new JTextField(probSrBr.toString());
        this.centerPanel.add(this.textField5);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 6
        this.description6 = new JLabel("Stosunek wykształcenia średniego do całości");
        this.centerPanel.add(description6);
        this.textField6 = new JTextField(probsred.toString());
        this.centerPanel.add(this.textField6);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 7
        this.description7 = new JLabel("Stosunek wykształcenia wyższego do całości");
        this.centerPanel.add(description7);
        this.textField7 = new JTextField(probWyzs.toString());
        this.centerPanel.add(this.textField7);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(probPodst.toString());
            this.textField2.setText(probGimn.toString());
            this.textField3.setText(probZasa.toString());
            this.textField4.setText(probZaBr.toString());
            this.textField5.setText(probSrBr.toString());
            this.textField6.setText(probsred.toString());
            this.textField7.setText(probWyzs.toString());
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newProbPodst = Double.parseDouble(this.textField1.getText());
                double newProbGim = Double.parseDouble(this.textField2.getText());
                double newProbZasa = Double.parseDouble(this.textField3.getText());
                double newProbZaBr = Double.parseDouble(this.textField4.getText());
                double newProbSrBr = Double.parseDouble(this.textField5.getText());
                double newProbsred = Double.parseDouble(this.textField6.getText());
                double newProbWyzs = Double.parseDouble(this.textField7.getText());

                if (newProbPodst < 0 || newProbPodst > 1000000 ||
                        newProbGim < 0 || newProbGim > 1000000 ||
                        newProbZasa < 0 || newProbZasa > 1000000 ||
                        newProbZaBr < 0 || newProbZaBr > 1000000 ||
                        newProbSrBr < 0 || newProbSrBr > 1000000 ||
                        newProbsred < 0 || newProbsred > 1000000 ||
                        newProbWyzs < 0 || newProbWyzs > 1000000)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
                    return;
                }

                Global.CURRENT_PROPERTIES.getEducationProperties().probPodst = newProbPodst;
                Global.CURRENT_PROPERTIES.getEducationProperties().probGimn = newProbGim;
                Global.CURRENT_PROPERTIES.getEducationProperties().probZasa = newProbZasa;
                Global.CURRENT_PROPERTIES.getEducationProperties().probZaBr = newProbZaBr;
                Global.CURRENT_PROPERTIES.getEducationProperties().probSrBr = newProbSrBr;
                Global.CURRENT_PROPERTIES.getEducationProperties().probsred = newProbsred;
                Global.CURRENT_PROPERTIES.getEducationProperties().probWyzs = newProbWyzs;
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
            }
        }
    }
}
