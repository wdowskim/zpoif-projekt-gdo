package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class EyesPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private JLabel description4;
    private JTextField textField4;

    private JLabel description5;
    private JTextField textField5;

    private JLabel description6;
    private JTextField textField6;

    private Double probAmberDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probAmber;
    private Double probGreenDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probGreen;
    private Double probGreyDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probGrey;
    private Double probHazelDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probHazel;
    private Double probBlueDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probBlue;
    private Double probBrownDefault = Global.DEFAULT_PROPERTIES.getEyesProperties().probBrown;

    public EyesPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Stosunek oczu bursztynowych do całości");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(probAmberDefault.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Stosunek oczu zielonych do całości");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(probGreenDefault.toString());
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Stosunek oczu szarych do całości");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(probGreyDefault.toString());
        this.centerPanel.add(this.textField3);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 4
        this.description4 = new JLabel("Stosunek oczu piwnych do całości");
        this.centerPanel.add(description4);
        this.textField4 = new JTextField(probHazelDefault.toString());
        this.centerPanel.add(this.textField4);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 5
        this.description5 = new JLabel("Stosunek oczu niebieskich do całości");
        this.centerPanel.add(description5);
        this.textField5 = new JTextField(probBlueDefault.toString());
        this.centerPanel.add(this.textField5);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 6
        this.description6 = new JLabel("Stosunek oczu brązowych do całości");
        this.centerPanel.add(description6);
        this.textField6 = new JTextField(probAmberDefault.toString());
        this.centerPanel.add(this.textField6);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(probAmberDefault.toString());
            this.textField2.setText(probGreenDefault.toString());
            this.textField3.setText(probGreyDefault.toString());
            this.textField4.setText(probHazelDefault.toString());
            this.textField5.setText(probBlueDefault.toString());
            this.textField6.setText(probBrownDefault.toString());
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newAmberProb = Double.parseDouble(this.textField1.getText());
                double newGreenProb = Double.parseDouble(this.textField2.getText());
                double newGreyProb = Double.parseDouble(this.textField3.getText());
                double newHazelProb = Double.parseDouble(this.textField4.getText());
                double newBlueProb = Double.parseDouble(this.textField5.getText());
                double newBrownProb = Double.parseDouble(this.textField6.getText());

                if (newAmberProb < 0 || newAmberProb > 1000000 ||
                        newGreenProb < 0 || newGreenProb > 1000000 ||
                        newGreyProb < 0 || newGreyProb > 1000000 ||
                        newHazelProb < 0 || newHazelProb > 1000000 ||
                        newBlueProb < 0 || newBlueProb > 1000000 ||
                        newBrownProb < 0 || newBrownProb > 1000000)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
                    return;
                }

                Global.CURRENT_PROPERTIES.getEyesProperties().probAmber = newAmberProb;
                Global.CURRENT_PROPERTIES.getEyesProperties().probGreen = newGreenProb;
                Global.CURRENT_PROPERTIES.getEyesProperties().probGrey = newGreyProb;
                Global.CURRENT_PROPERTIES.getEyesProperties().probHazel = newHazelProb;
                Global.CURRENT_PROPERTIES.getEyesProperties().probBlue = newBlueProb;
                Global.CURRENT_PROPERTIES.getEyesProperties().probBrown = newBrownProb;
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
            }
        }
    }
}
