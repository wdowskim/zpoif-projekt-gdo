package GUI.PropertiesCards;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class PropertiesCard implements ActionListener {

    JPanel panel;
    JPanel centerPanel;
    JPanel bottomPanel;


    JButton setDefaultButton = new JButton("Wróc domyślne");
    JButton savePropertiesButton = new JButton("Zapisz właściwości");

    public JPanel getPanel()
    {
        return this.panel;
    }
}
