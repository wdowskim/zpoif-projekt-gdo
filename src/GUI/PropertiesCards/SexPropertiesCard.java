package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class SexPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private Double defaultMaleFraction = Global.DEFAULT_PROPERTIES.getSexProperties().maleFraction;
    private String defaultMaleSymbol = Global.DEFAULT_PROPERTIES.getSexProperties().maleSymbol;
    private String defaultFemaleSymbol = Global.DEFAULT_PROPERTIES.getSexProperties().femaleSymbol;

    public SexPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Frakcja mężczyzn w populacji");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(defaultMaleFraction.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Symbol oznaczenia mężczyzn");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(defaultMaleSymbol);
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Symbol oznaczenia kobiet");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(defaultFemaleSymbol);
        this.centerPanel.add(this.textField3);

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(defaultMaleFraction.toString());
            this.textField2.setText(defaultMaleSymbol);
            this.textField3.setText(defaultFemaleSymbol);
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newMaleFraction = Double.parseDouble(this.textField1.getText());
                if (newMaleFraction < 0 || newMaleFraction > 1)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji.\nPodaj liczbę rzeczywistą od 0 do 1.");
                    return;
                }
                Global.CURRENT_PROPERTIES.getSexProperties().maleFraction = newMaleFraction;
                Global.CURRENT_PROPERTIES.getSexProperties().maleSymbol = this.textField2.getText();
                Global.CURRENT_PROPERTIES.getSexProperties().femaleSymbol = this.textField3.getText();
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji.\nPodaj liczbę rzeczywistą od 0 do 1.");
            }
        }
    }
}
