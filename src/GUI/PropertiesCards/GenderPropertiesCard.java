package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class GenderPropertiesCard extends PropertiesCard{

    JLabel description1;
    JTextField textField1;

    JLabel description2;
    JTextField textField2;

    JLabel description3;
    JTextField textField3;

    private Double defaultGenderDysphoriaProbability = Global.DEFAULT_PROPERTIES.getGenderProperties().genderDysphoriaProbability;
    private Double defaultOtherGenderProbability = Global.DEFAULT_PROPERTIES.getGenderProperties().otherGenderProbability;
    private String defaultOtherSymbol = Global.DEFAULT_PROPERTIES.getGenderProperties().otherSymbol;

    public GenderPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(20, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Frakcja osób z dysforią płciową");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(defaultGenderDysphoriaProbability.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Frakcja osób z niebinarną płcią społeczną");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(defaultOtherGenderProbability.toString());
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Symbol oznaczenia osób niebinarnych");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(defaultOtherSymbol);
        this.centerPanel.add(this.textField3);

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(defaultGenderDysphoriaProbability.toString());
            this.textField2.setText(defaultOtherGenderProbability.toString());
            this.textField3.setText(defaultOtherSymbol);
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newGenderDysphoriaProbability = Double.parseDouble(this.textField1.getText());
                double newOtherGenderProbability = Double.parseDouble(this.textField2.getText());
                if (newGenderDysphoriaProbability < 0 || newGenderDysphoriaProbability > 1 || newOtherGenderProbability < 0 || newOtherGenderProbability > 1)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji.\nPodaj liczbę rzeczywistą od 0 do 1.");
                    return;
                }
                Global.CURRENT_PROPERTIES.getGenderProperties().genderDysphoriaProbability = newGenderDysphoriaProbability;
                Global.CURRENT_PROPERTIES.getGenderProperties().otherGenderProbability = newOtherGenderProbability;
                Global.CURRENT_PROPERTIES.getGenderProperties().otherSymbol = this.textField3.getText();
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format frakcji mężczyzn.\nPodaj liczbę rzeczywistą od 0 do 1.");
            }
        }
    }
}
