package GUI.PropertiesCards;

import Base.Global;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class WeightPropertiesCard extends PropertiesCard {

    private JLabel description1;
    private JTextField textField1;

    private JLabel description2;
    private  JTextField textField2;

    private JLabel description3;
    private JTextField textField3;

    private Double probNormDefault = Global.DEFAULT_PROPERTIES.getWeightProperties().probNorm;
    private Double probUnderDefault = Global.DEFAULT_PROPERTIES.getWeightProperties().probUnder;
    private Double probOverDefault = Global.DEFAULT_PROPERTIES.getWeightProperties().probOver;

    public WeightPropertiesCard()
    {
        // Postawienie listenerów przycisków
        this.setDefaultButton.addActionListener(this);
        this.savePropertiesButton.addActionListener(this);

        // Utworzenie paneli i przycisków na dole panelu
        this.panel = new JPanel(new BorderLayout());
        this.bottomPanel = new JPanel(new FlowLayout());
        this.bottomPanel.add(setDefaultButton);
        this.bottomPanel.add(savePropertiesButton);
        this.panel.add(this.bottomPanel, BorderLayout.PAGE_END);
        this.centerPanel = new JPanel(new GridLayout(22, 1));

        // Utworzenie kontrolek do danej karty
        // 1
        this.description1 = new JLabel("Stosunek wagi poprawnej do całości");
        this.centerPanel.add(description1);
        this.textField1 = new JTextField(probNormDefault.toString());
        this.centerPanel.add(this.textField1);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 2
        this.description2 = new JLabel("Stosunek niedowagi do całości");
        this.centerPanel.add(description2);
        this.textField2 = new JTextField(probUnderDefault.toString());
        this.centerPanel.add(this.textField2);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // 3
        this.description3 = new JLabel("Stosunek nadwagi do całości");
        this.centerPanel.add(description3);
        this.textField3 = new JTextField(probOverDefault.toString());
        this.centerPanel.add(this.textField3);
        this.centerPanel.add(Box.createRigidArea(new Dimension(30, 10)));

        // Dodanie centralnego panelu do panelu głównego
        this.panel.add(this.centerPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource() == this.setDefaultButton)
        {
            this.textField1.setText(probNormDefault.toString());
            this.textField2.setText(probUnderDefault.toString());
            this.textField3.setText(probOverDefault.toString());
        }
        if (e.getSource() == this.savePropertiesButton)
        {
            try
            {
                double newProbPodst = Double.parseDouble(this.textField1.getText());
                double newProbGim = Double.parseDouble(this.textField2.getText());
                double newProbZasa = Double.parseDouble(this.textField3.getText());

                if (newProbPodst < 0 || newProbPodst > 1000000 ||
                        newProbGim < 0 || newProbGim > 1000000 ||
                        newProbZasa < 0 || newProbZasa > 1000000)
                {
                    JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
                    return;
                }

                Global.CURRENT_PROPERTIES.getEducationProperties().probPodst = newProbPodst;
                Global.CURRENT_PROPERTIES.getEducationProperties().probGimn = newProbGim;
                Global.CURRENT_PROPERTIES.getEducationProperties().probZasa = newProbZasa;
            }
            catch (Exception exception)
            {
                JOptionPane.showMessageDialog(this.panel,"Niewłaściwy format stosunku.\nPodaj liczbę rzeczywistą od 0 do 1 mln.");
            }
        }
    }
}
