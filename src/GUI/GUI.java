package GUI;

import Attributes.*;
import Base.Global;
import Base.Record;
import GUI.PropertiesCards.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class GUI implements ActionListener, ItemListener {

    private LinkedHashMap<String, JCheckBox> attributeChoices;

    private JFrame mainFrame;
    private JFrame finalFrame;
    private JFrame onGenerateFrame;

    private JPanel topPanel;
    private JPanel attributesListPanel;
    private JPanel propertiesCardsPanel;
    private JPanel bottomPanel;
    private JPanel cardsPanel;

    private JButton aboutButton;
    private JButton helpButton;
    private JButton finalizeButton;
    private JButton generateButton;
    private JButton chooseFilePathButton;
    private JButton checkAllAttributes;

    private JTextField numberOfRecordsTextField;
    private JTextField fileNameTextField;

    private JComboBox chosenFileTypeComboBox;

    private JFileChooser filePathChooser;

    public GUI()
    {
        // Zainicjowanie listy checkboxów z zaznaczonymi atrybutami
        this.attributeChoices = new LinkedHashMap<>();

        // Zainicjowanie mainFrame - głównej ramki na wszytskie panele
        this.mainFrame = new JFrame("Generator sztucznych danych");

        // Ustawienie zapytania o potwierdzenie wyjścia z programu
        this.mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.mainFrame.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent we)
            {
                Object[] options = {"Tak", "Nie"};
                int result = JOptionPane.showOptionDialog(mainFrame, "Czy na pewno chcesz wyjść?", "Wychodzenie z programu",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

                if (result == JOptionPane.YES_OPTION)
                {
                    mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
                else if (result == JOptionPane.NO_OPTION)
                {
                    mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                }
            }
        });

        // Zainicjowanie topPanel - panelu na przyciski na górze głównej ramki
        this.topPanel = new JPanel(new FlowLayout());

        // Zainicjowanie przycisku aboutButton
        this.aboutButton = new JButton("O programie");
        this.aboutButton.addActionListener(this);
        this.topPanel.add(this.aboutButton);

        // Zainicjowanie przycisku aboutButton
        this.helpButton = new JButton("Pomoc");
        this.helpButton.addActionListener(this);
        this.topPanel.add(this.helpButton);

        // Dodanie topPanel do mainFrame
        this.mainFrame.add(this.topPanel, BorderLayout.PAGE_START);


        // Zainicjowanie attributesListPanel - panelu na listę z atrybutami
        this.attributesListPanel = this.createAttributesListPanel();
        this.mainFrame.add(this.attributesListPanel, BorderLayout.LINE_START);


        // Zainicjowanie propertiesCardsPanel - panelu ze zmienianymi kartami, na któ©ych można wybrać właściwości
        this.propertiesCardsPanel = this.createPropertiesCardsPanel();
        this.mainFrame.add(this.propertiesCardsPanel, BorderLayout.CENTER);


        // Zainicjowanie bottomPanel - panelu na dole głównej ramki
        this.bottomPanel = new JPanel(new FlowLayout());

        // Dodanie przycisku generateButton - do kończenia przyjmowania danych i ustalenia ostatecnzych ustawień
        this.finalizeButton = new JButton("Finalizuj wybór opcji i generuj dane");
        this.finalizeButton.addActionListener(this);
        this.bottomPanel.add(this.finalizeButton);

        // Dodanie bottomPanel do mainFrame
        this.mainFrame.add(this.bottomPanel, BorderLayout.PAGE_END);


        // Dopasowanie rozmiarów ramki i włączenie jej wyświetlania
        this.mainFrame.pack();
        this.mainFrame.setLocationRelativeTo(null);
        this.mainFrame.setVisible(true);
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e)
    {
        // Działanie dla kliknięcia aboutButton
        if (e.getSource() == this.aboutButton)
        {
            JOptionPane.showMessageDialog(this.mainFrame, Global.ABOUT_MESSAGE_TEXT, "O programie", JOptionPane.INFORMATION_MESSAGE);
        }

        // Działanie dla kliknięcia helpButton
        if (e.getSource() == this.helpButton)
        {
            JOptionPane.showMessageDialog(this.mainFrame, Global.HELP_MESSAGE_TEXT, "Pomoc", JOptionPane.INFORMATION_MESSAGE);
        }

        // Działanie dla kliknięcia finalizeButton
        if (e.getSource() == this.finalizeButton)
        {
            // TODO: najpwierw spawdźmy czy dane wejściowe są poprawne, jak są to ok, jak nie są to jakiś error i wypad
            // ogólnie to się sprawdzają na bieżąco, więc chyba ok

            // Wyłącz klikanie w głównej ramce
            this.mainFrame.setEnabled(false);

            // Włącz końcową ramkę, która dobierze
            this.finalFrame = this.createFinalFrame();
        }

        // Działanie dla kliknięcia przycisku wyboru ściezki do pliku
        if (e.getSource() == this.chooseFilePathButton)
        {
            this.filePathChooser = new JFileChooser();
            this.filePathChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            this.filePathChooser.setAcceptAllFileFilterUsed(false);
            this.filePathChooser.setDialogTitle("Wybór folderu do zapisu");
            if (this.filePathChooser.showOpenDialog(this.filePathChooser) == JFileChooser.APPROVE_OPTION)
            {
                this.chooseFilePathButton.setText(this.filePathChooser.getSelectedFile().toString());
            }
        }

        // Generowanie danych
        if (e.getSource() == this.generateButton)
        {
            // wyłaczenie ramki
            this.finalFrame.setEnabled(false);
            // Pokazanie ramki na czas tworzenia
            //this.onGenerateFrame = this.createOnGenerateFrame();

            try {
                // Wzięcie finalizujących ustawień
                String fileName = this.fileNameTextField.getText();
                String chosenFileType = (String) this.chosenFileTypeComboBox.getSelectedItem();
                int quantityToGenerate = Integer.parseInt(this.numberOfRecordsTextField.getText());
                if (quantityToGenerate < 1 || quantityToGenerate > 10000000) {
                    JOptionPane.showMessageDialog(this.onGenerateFrame, "Błąd w liczbie rekordów do generowania.\nPodaj liczbę całkowitą dodatnią, mniejszą od 10 mln");
                    // włączenie ramki spowrotem
                    //this.onGenerateFrame.setVisible(false);
                    this.finalFrame.setEnabled(true);
                    return;
                }
                String chosenFilePath = this.filePathChooser.getSelectedFile().toString();

                // Przekazanie wybranych atrybutów do CHOSEN_ATTRIBUTES
                Global.CHOSEN_ATTRIBUTES.clear();
                for (Map.Entry<String, JCheckBox> element : this.attributeChoices.entrySet())
                {
                    Global.CHOSEN_ATTRIBUTES.put(element.getKey(), element.getValue().isSelected());
                }

                // Tworzenie generatorów rekordów
                // tworzenie nowych atrybutów
                Sex sex = new Sex();
                Gender gender = new Gender();
                FirstName firstName = new FirstName("resources/DataFN.csv", "resources/DataMN.csv");
                MiddleName middleName = new MiddleName("resources/DataFN.csv", "resources/DataMN.csv");
                Surname surname = new Surname("resources/DataFS.csv", "resources/DataMS.csv");
                BirthMonth birthMonth = new BirthMonth();
                BirthYear birthYear = new BirthYear("resources/DataFA.csv", "resources/DataMA.csv");
                BirthDay birthDay = new BirthDay();
                Pesel pesel = new Pesel();
                Height height = new Height();
                Weight weight = new Weight();
                PhoneNumber phoneNumber = new PhoneNumber();
                Email email = new Email();
                MaritalStatus maritalStatus = new MaritalStatus();
                Eyes eyes = new Eyes();
                Education education = new Education();
                Hair hair = new Hair();
                Denomination denomination = new Denomination();
                FatherName fatherName = new FatherName("resources/DataMN.csv");
                MotherName motherName = new MotherName("resources/DataFN.csv");
                MaidenName maidenName = new MaidenName("resources/DataFS.csv");
                Job job = new Job();
                NrDowodu nrDowodu = new NrDowodu();
                State state = new State("resources/Wojew.csv");
                Powiat powiat = new Powiat();
                Gmina gmina = new Gmina();

                // tworzenie mapy z danymi wejściowymi, docelowo podanymi przez GUI, i dodanie atrybutów
                Map<String, Attribute> entry = new LinkedHashMap<>();
                entry.put("Sex", sex);
                entry.put("Gender", gender);
                entry.put("FirstName", firstName);
                entry.put("MiddleName", middleName);
                entry.put("Surname", surname);
                entry.put("BirthYear", birthYear);
                entry.put("BirthMonth", birthMonth);
                entry.put("BirthDay", birthDay);
                entry.put("Pesel", pesel);
                entry.put("Height", height);
                entry.put("Weight", weight);
                entry.put("PhoneNumber", phoneNumber);
                entry.put("Email", email);
                entry.put("MaritalStatus", maritalStatus);
                entry.put("Eyes", eyes);
                entry.put("Education", education);
                entry.put("Hair", hair);
                entry.put("Denomination", denomination);
                entry.put("FatherName", fatherName);
                entry.put("MotherName", motherName);
                entry.put("MaidenName", maidenName);
                entry.put("Job", job);
                entry.put("NrDowodu", nrDowodu);
                entry.put("State", state);
                entry.put("Powiat", powiat);
                entry.put("Gmina", gmina);

                // utworzenie rekordów
                Record r = new Record(entry);
                // wygenerowanie wyników

                String finalPath = chosenFilePath + File.separator + fileName + "." + chosenFileType;
                System.out.println(finalPath);

                r.generateToFile(quantityToGenerate, chosenFileType, finalPath);
                System.out.println("DONE");
            }
            catch (Exception exception)
            {
                exception.printStackTrace();
                JOptionPane.showMessageDialog(this.onGenerateFrame, "Wystąpił błąd, możliwe problemy to:\n- liczba rekordów nie jest całkowitą z zakresu 1 do 10 mln\n-Problem z dostaniem się do danych źródłowych\n-Brak dostępu do docelowego miejsca zapisu pliku\n-Nieprawidłowa nazwa docelowego pliku");
            }

            // właczenie ramki spowrotem
            //this.onGenerateFrame.setVisible(false);
            this.finalFrame.setEnabled(true);
        }

        if (e.getSource() == this.checkAllAttributes)
        {
            System.out.println("elo");
            JCheckBox currentCheckbox;
            for (Map.Entry<String, JCheckBox> element : this.attributeChoices.entrySet())
            {
                currentCheckbox = element.getValue();
                currentCheckbox.setSelected(true);
                element.setValue(currentCheckbox);
            }
        }
    }

    public static void main(String[] args)
    {
        System.out.println("cośtam");
        GUI gui = new GUI();
    }

    @Override
    public void itemStateChanged(@NotNull ItemEvent e)
    {
        // Kontrola zmian kart w wyborze włąsciwości atrybutów
        CardLayout cl = (CardLayout) (cardsPanel.getLayout());
        cl.show (cardsPanel, (String) e.getItem());
    }

    @NotNull
    private JPanel createAttributesListPanel()
    {
        // Metoda do stworzenia panelu z listą checkboxów na atrybuty do wybrania

        // Inicjowanie panelu i boxa z pionowym układaniem elementów
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        Box box = Box.createVerticalBox();

        // Dodanie napisu na górze
        panel.add(new JLabel("Atrybuty:"), BorderLayout.PAGE_START);

        // Stworzenie checkboxów na zaznaczanie atrybutów i dodanie ich do boxa na atrubuty
        for (Map.Entry<String, String> attributeName : Global.ATTRIBUTES__NAMES.entrySet())
        {
            this.attributeChoices.put(attributeName.getKey(), new JCheckBox(attributeName.getValue()));
            box.add(this.attributeChoices.get(attributeName.getKey()));
        }

        //JPanel panelForButton = new JPanel(new FlowLayout());
        this.checkAllAttributes = new JButton("Zaznacz wszystko");
        //panelForButton.add(this.checkAllAttributes);
        this.checkAllAttributes.addActionListener(this);
        box.add(this.checkAllAttributes);

        // Dodanie możliwości scrollowania
        JScrollPane scrollPane = new JScrollPane(box);
        scrollPane.setPreferredSize(new Dimension(320, 640));
        panel.add(scrollPane, BorderLayout.CENTER);

        return panel;
    }

    @NotNull
    private JPanel createPropertiesCardsPanel()
    {
        // Metoda do stworzenia panelu z ustawieniami właściwości danego atrybutu

        // Inicjowanie panelu i ustawienie layoutu
        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());

        // Stworzenie comboboxa z wartościami - nazwami atrybutów
        Collection<String> values = Global.ATTRIBUTES__NAMES.values();
        String[] targetArray = values.toArray(new String[0]);
        JComboBox comboBox = new JComboBox(targetArray);

        // Postawienie listenera dla comboboxa
        comboBox.addItemListener((ItemListener) this);

        // Wstawienie comboboxa do panelu
        panel.add(comboBox, BorderLayout.PAGE_START);

        // Ustawienie panelu na karty
        this.cardsPanel = new JPanel(new CardLayout());

        // Ustawienie kart na poszczególne atrybuty
        cardsPanel.add((new SexPropertiesCard()).getPanel(), "Płeć biologiczna (Sex)");
        cardsPanel.add(new GenderPropertiesCard().getPanel(), "Płeć społeczna (Gender)");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Imię");
        cardsPanel.add(new MiddleNamePropertiesCard().getPanel(), "Drugie imię");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Nazwisko");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Rok urodzenia");
        cardsPanel.add(new BirthMonthPropertiesCard().getPanel(), "Miesiąc urodzenia");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Dzień urodzenia");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Numer PESEL");
        cardsPanel.add(new PhoneNumberPropertiesCard().getPanel(), "Numer telefonu");
        cardsPanel.add(new EmailPropertiesCard().getPanel(), "Adres e-mail");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Wzrost");
        cardsPanel.add(new WeightPropertiesCard().getPanel(), "Waga");
        cardsPanel.add(new EyesPropertiesCard().getPanel(), "Kolor oczu");
        cardsPanel.add(new HairPropertiesCard().getPanel(), "Kolor włosów");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Stan cywilny");
        cardsPanel.add(new EducationPropertiesCard().getPanel(), "Stopień wykształcenia");
        cardsPanel.add(new DenominationPropertiesCard().getPanel(), "Wyznanie");
        cardsPanel.add(new OrientationPropertiesCard().getPanel(), "Orientacja seksualna");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Imię ojca");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Imię matki");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Nazwisko rodowe");
        cardsPanel.add(new JobPropertiesCard().getPanel(), "Stan zatrudnienia");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Numer dowodu");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Województwo");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Powiat");
        cardsPanel.add(new NoPropertiesCard().getPanel(), "Gmina");

        panel.add(cardsPanel, BorderLayout.CENTER);
        JScrollPane scrollPane = new JScrollPane(panel);
        scrollPane.setPreferredSize(new Dimension(480, 640));

        // ustawienie ostatecznego panelu
        JPanel finalPanel = new JPanel();
        finalPanel.setLayout(new BorderLayout());
        finalPanel.add(new JLabel("Właściowści atrybutów"), BorderLayout.PAGE_START);
        finalPanel.add(scrollPane, BorderLayout.CENTER);

        return finalPanel;
    }

    @NotNull
    private JFrame createFinalFrame()
    {
        // Inicjowanie nowej ramki
        JFrame frame = new JFrame("Finalizowanie");

        // Ustawienie że ona znika jak się ją zamknie, a jednocześnie przy tym włączy się spowrotem ramka mainFrame
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                mainFrame.setEnabled(true);
                frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });


        // Inicjowanie panelu na opisy
        JPanel descriptionsPanel = new JPanel(new GridLayout(4, 1));

        // Opisy
        descriptionsPanel.add(new JLabel("\nLiczba rekordów:\n"));
        descriptionsPanel.add(new JLabel("Format pliku:\n"));
        descriptionsPanel.add(new JLabel("Folder do zapisu pliku:\n"));
        descriptionsPanel.add(new JLabel("Nazwa pliku:\n"));

        // Dodanie panelu z opisami do ramki
        frame.add(descriptionsPanel, BorderLayout.LINE_START);


        // Inicjowanie panelu na kontrolki
        JPanel controlsPanel = new JPanel(new GridLayout(4, 1));

        // Dodanie okna na wpisanie liczby rekordów
        this.numberOfRecordsTextField = new JTextField("10000");
        controlsPanel.add(this.numberOfRecordsTextField);

        // Dodanie okna na podanie rozszerzenia pliku
        this.chosenFileTypeComboBox = new JComboBox(Global.FILE_TYPES);
        controlsPanel.add(this.chosenFileTypeComboBox);

        // Dodanie szukania folderu docelowego
        this.chooseFilePathButton = new JButton("kliknij aby wybrać");
        this.chooseFilePathButton.addActionListener(this);
        controlsPanel.add(this.chooseFilePathButton);

        // Dodanie okna na wpisanie liczby rekordów
        this.fileNameTextField = new JTextField("output");
        controlsPanel.add(this.fileNameTextField);

        // Dodanie panelu z kontrolkami do ramki
        frame.add(controlsPanel, BorderLayout.CENTER);


        // Inicjalizacja panelu na dole ramki na przycisk kończący
        JPanel bottomPanel = new JPanel(new FlowLayout());

        // Inicjalizacja przycisku na generowanie danych i ustawienie listenera
        this.generateButton = new JButton("Generuj dane");
        this.generateButton.addActionListener(this);
        bottomPanel.add(this.generateButton);

        // Dodanie panelu dolnego do ramki
        frame.add(bottomPanel, BorderLayout.PAGE_END);

        // Finalizacja stawiania ramki
        frame.setSize(400, 200);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        return frame;
    }

    @NotNull
    private JFrame createOnGenerateFrame()
    {
        JFrame frame = new JFrame("Generowanie danych");
        JPanel panel = new JPanel(new GridLayout(2, 1));

        panel.add(new JLabel("Trwa tworzenie pliku..."));
        panel.add(new JLabel("To może chwilę potrwać..."));
        frame.add(panel, BorderLayout.CENTER);

        frame.setLocationRelativeTo(null);
        frame.setSize(new Dimension(220, 100));
        frame.setVisible(true);

        return frame;
    }
}