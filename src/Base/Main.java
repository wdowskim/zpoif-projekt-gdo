package Base;

import GUI.GUI;

public class Main {
    public static void main (String[] args) throws Exception {

        GUI gui = new GUI();
        /*
        Person test1 = new Person();
        System.out.println(test1.surname);
        System.out.println(test1.name);
        System.out.println(test1.birth_day);
        System.out.println(test1.birth_month);
        System.out.println(test1.birth_year);
        System.out.println(test1.pesel);
        */

        /*
        Random random = new Random();
        // tworzenie nowych atrybutów
        Sex sex = new Sex();
        Gender gender = new Gender();
        FirstName firstName = new FirstName("resources/DataFN.csv", "resources/DataMN.csv");
        MiddleName middleName = new MiddleName("resources/DataFN.csv", "resources/DataMN.csv");
        Surname surname = new Surname("resources/DataFS.csv", "resources/DataMS.csv");
        BirthMonth birthMonth = new BirthMonth();
        BirthYear birthYear = new BirthYear("resources/DataFA.csv","resources/DataMA.csv");
        BirthDay birthDay = new BirthDay();
        Pesel pesel = new Pesel();
        Height height = new Height();
        Weight weight = new Weight();
        PhoneNumber phoneNumber = new PhoneNumber();
        MaritalStatus maritalStatus = new MaritalStatus();
        Eyes eyes = new Eyes();
        Education education = new Education();
        Hair hair = new Hair();
        Orientation orientation = new Orientation();
        Denomination denomination = new Denomination();
        Job job = new Job();
        NrDowodu nrDowodu = new NrDowodu();
        State state = new State("resources/Wojew.csv");
        Powiat powiat = new Powiat();
        Gmina gmina = new Gmina();

        // tworzenie mapy z danymi wejściowymi, docelowo podanymi przez GUI, i dodanie atrybutów
        Map<String, Attribute> entry = new LinkedHashMap<>();
        entry.put("Sex", sex);
        entry.put("Gender", gender);
        entry.put("FirstName", firstName);
        entry.put("MiddleName", middleName);
        entry.put("Surname", surname);
        entry.put("BirthYear", birthYear);
        entry.put("BirthMonth", birthMonth);
        entry.put("BirthDay", birthDay);
        entry.put("Pesel",pesel);
        entry.put("Height",height);
        entry.put("Weight",weight);
        entry.put("PhoneNumber",phoneNumber);
        entry.put("MaritalStatus",maritalStatus);
        entry.put("Eyes", eyes);
        entry.put("Education",education);
        entry.put("Hair",hair);
        entry.put("Orientation",orientation);
        entry.put("Denomiantion",denomination);
        entry.put("Job",job);
        entry.put("NrDowodu",nrDowodu);
        entry.put("State",state);
        entry.put("Powiat",powiat);
        entry.put("Gmina",gmina);


        // utworzenie rekordów
        Record r = new Record(entry);
        // wygenerowanie wyników
        System.out.println(r.generate(100));

        //Testowanie zapisu do pliku
        //r.generateToFile(100, "xml", "/home/michal/Desktop/file.xml");

        //Zczytywanie pliku z git'a
        /*
        File datafs = new File("resources/DataFN.csv"); //Releative Path of a file
        BufferedReader readfs = new BufferedReader(new FileReader(datafs));
        String currentlinefs = readfs.readLine();
        System.out.println(currentlinefs);
        currentlinefs = readfs.readLine();
        System.out.println(currentlinefs);
        currentlinefs = readfs.readLine();
        System.out.println(currentlinefs);
        System.out.println(Math.random());
        */

        // Test wczytywania imion
        /*
        String tt = "aaaa,2137";
        System.out.println(tt.split(",")[1]);
        ProbabilityFromFile pff = new ProbabilityFromFile("resources/DataFN.csv");
        System.out.println(pff.generate());
        System.out.println(pff.generate());
        System.out.println(pff.generate());
        System.out.println(pff.generate());
        */
    }
}
