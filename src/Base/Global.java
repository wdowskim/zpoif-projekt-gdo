package Base;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

public class Global {
    // klasa przechowuje imitowane zmienne globalne

    // wzorzec oznaczenia braku danych
    public static String EMPTY_STRING = "";

    // Nazwy klas i odpowiadające im nazwy w języku polskim
    public static Map<String, String> ATTRIBUTES__NAMES;
    static
    {
        Map<String, String> tmp = new LinkedHashMap<String, String>();

        tmp.put("Sex", "Płeć biologiczna (Sex)");
        tmp.put("Gender", "Płeć społeczna (Gender)");
        tmp.put("FirstName", "Imię");
        tmp.put("MiddleName", "Drugie imię");
        tmp.put("Surname", "Nazwisko");
        tmp.put("BirthYear", "Rok urodzenia");
        tmp.put("BirthMonth", "Miesiąc urodzenia");
        tmp.put("BirthDay", "Dzień urodzenia");
        tmp.put("Pesel", "Numer PESEL");
        tmp.put("PhoneNumber", "Numer telefonu");
        tmp.put("Email", "Adres e-mail");
        tmp.put("Height", "Wzrost");
        tmp.put("Weight", "Waga");
        tmp.put("Eyes", "Kolor oczu");
        tmp.put("Hair", "Kolor włosów");
        tmp.put("MaritalStatus", "Stan cywilny");
        tmp.put("Education", "Stopień wykształcenia");
        tmp.put("Denomination", "Wyznanie");
        tmp.put("Orientation", "Orientacja seksualna");
        tmp.put("FatherName", "Imię ojca");
        tmp.put("MotherName", "Imię matki");
        tmp.put("MaidenName", "Nazwisko rodowe");
        tmp.put("Job", "Stan zatrudnienia");
        tmp.put("NrDowodu", "Numer dowodu");
        tmp.put("State", "Województwo");
        tmp.put("Powiat", "Powiat");
        tmp.put("Gmina", "Gmina");

        ATTRIBUTES__NAMES = tmp;
    }

    // Tekst "O programie"
    public static final String ABOUT_MESSAGE_TEXT = "Generator sztucznych danych\n\n Autorzy:\nMichał Wdowski\nMarcin Łukaszyk\n\nProgram został napisany w ramach projektu na ZPOiF w semestrze zimowym 2019/2020";

    // Tekst pomocy
    public static final String HELP_MESSAGE_TEXT = "- Wybierz atrybuty (kolumny), które mają się pojawić w danych\n- Wybierz właściwości danych w wybranej kolumnie\n- Po wyborze właściwości zapisz swoje wybory\n- Możesz przywrócić domyślne wartości. Pamiętaj o ich zapisaniu!\n- Finalizuj generowanie i wybierz liczbe rekordów, format danych i miejsce do zapisu";

    // Dostępne typy danych
    public static final String[] FILE_TYPES = {"csv", "tsv", "json", "xml"};

    // Domyślne wartości właściwości atrybotów
    public static final PropertiesContainer DEFAULT_PROPERTIES = new PropertiesContainer();

    // Aktualne ustawienia właściwości atrybutów
    public static PropertiesContainer CURRENT_PROPERTIES = new PropertiesContainer();

    // Ustawienia na wybrane kolumny
    public static LinkedHashMap<String, Boolean> CHOSEN_ATTRIBUTES = new LinkedHashMap<>();

}
