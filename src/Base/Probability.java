package Base;

public class Probability {

    @SafeVarargs
    public static <T> T discreteUniform(T...args)
    {
        // metoda wybiera z rozkładem jednostajnym dyskretnym jeden element z podanych
        int index = (int) (Math.random()*args.length);
        return args[index];
    }

    public static boolean happensWithProbability(double probability)
    {
        // metoda wykorzystywana w sytuacji, gdy chcemy sprawdzić zdarzenie zajdzie, jeśli mamy daną wartość prawdopodobieństwa
        return Math.random() < probability;
    }

}
