package Base;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Map;

public class PropertiesContainer {

    private Map<String, Properties> properties = new LinkedHashMap<>();

    public PropertiesContainer()
    {
        this.properties.put("Sex", new SexProperties());
        this.properties.put("Gender", new GenderProperties());
        this.properties.put("MiddleName", new MiddleNameProperties());
        this.properties.put("PhoneNumber", new PhoneNumberProperties());
        this.properties.put("Eyes",new EyesProperties());
        this.properties.put("Education",new EducationProperties());
        this.properties.put("Hair",new HairProperties());
        this.properties.put("Weight",new WeightProperties());
        this.properties.put("Denomination",new DenominationProperties());
        this.properties.put("Orientation",new OrientationProperties());
        this.properties.put("Job",new JobPropeties());
        this.properties.put("BirthMonth", new BirthMonthProperties());
        this.properties.put("Email", new EmailProperties());
    }

    public abstract class Properties {}

    // SexProperties
    public class SexProperties extends Properties {
        public double maleFraction = 0.5;
        public String maleSymbol = "M";
        public String femaleSymbol = "F";
    }

    public SexProperties getSexProperties()
    {
        return (SexProperties) this.properties.get("Sex");
    }

    // GenderProperties
    public class GenderProperties extends Properties {
        public double genderDysphoriaProbability = 0.03;
        public double otherGenderProbability = 0.01;
        public String otherSymbol = "O";
    }

    public GenderProperties getGenderProperties()
    {
        return (GenderProperties) this.properties.get("Gender");
    }


    // MiddleNameProperties
    public class MiddleNameProperties extends Properties {
        public double middleNameFraction = 0.5;
    }

    public MiddleNameProperties getMiddleNameProperties()
    {
        return (MiddleNameProperties) this.properties.get("MiddleName");
    }


    // PhoneNumberProperties
    public class PhoneNumberProperties extends Properties {
        public double phoneNumberFraction = 0.7;
    }

    public PhoneNumberProperties getPhoneNumberProperties()
    {
        return (PhoneNumberProperties) this.properties.get("PhoneNumber");
    }


    // EyesProperties
    public class EyesProperties extends Properties{
        public double probAmber = 1.0;
        public double probGreen = 1.0;
        public double probGrey = 1.0;
        public double probHazel = 1.0;
        public double probBlue = 1.0;
        public double probBrown = 1.0;
    }
    public EyesProperties getEyesProperties(){
        return (EyesProperties) this.properties.get("Eyes");
    }


    // EducationProperties
    public class EducationProperties extends Properties{
        public double probPodst = 0.1;
        public double probGimn = 0.15;
        public double probZasa = 0.25;
        public double probZaBr = 0.1;
        public double probSrBr = 0.2;
        public double probsred = 0.4;
        public double probWyzs = 0.6;
    }
    public EducationProperties getEducationProperties(){
        return (EducationProperties) this.properties.get("Education");
    }


    // HairProperties
    public class HairProperties extends Properties{
        public double probBrown = 1;
        public double probBlack = 1;
        public double probBlond = 1;
        public double probAuburn = 1;
        public double probRed =1;
        public boolean allowGray = true;
    }
    public HairProperties getHairProperties(){
        return (HairProperties) this.properties.get("Hair");
    }


    // WeightProperties
    public class WeightProperties extends Properties{
        public double probUnder = 1.0;
        public double probNorm = 1.0;
        public double probOver = 1.0;
    }
    public WeightProperties getWeightProperties(){
        return (WeightProperties) this.properties.get("Weight");
    }


    // BirthMonthProperties
    public class BirthMonthProperties extends Properties{
        public String januarySymbol = "sty";
        public String februarySymbol = "lut";
        public String marchSymbol = "mar";
        public String aprilSymbol = "kwi";
        public String maySymbol = "maj";
        public String juneSymbol = "cze";
        public String julySymbol = "lip";
        public String augustSymbol = "sie";
        public String septemberSymbol = "wrz";
        public String octoberSymbol = "paź";
        public String novemberSymbol = "lis";
        public String decemberSymbol = "gru";
    }
    public BirthMonthProperties getBirthMonthProperties(){
        return (BirthMonthProperties) this.properties.get("BirthMonth");
    }
    public class DenominationProperties extends Properties{
        public double probAteis = 1.0;
        public double probChrist = 2.0;
        public double probHindu = 0.2;
        public double probJudai = 0.3;
        public double probMuslim = 0.1;
    }
    public DenominationProperties getDenominationProperties(){
        return (DenominationProperties) this.properties.get("Denomination");
    }
    public class OrientationProperties extends Properties{
        public double probHetero = 1.5;
        public double probGay = 0.5;
        public double probBi = 0.2;
        public double probAce = 1;
    }
    public OrientationProperties getOrientationProperties(){
        return (OrientationProperties) this.properties.get("Orientation");
    }
    public class JobPropeties extends Properties{
        public double probJob = 0.9;
    }
    public JobPropeties getJobPropeties(){
        return (JobPropeties) this.properties.get("Job");
    }


    // PhoneNumberProperties
    public class EmailProperties extends Properties {
        public double emailFraction = 0.7;
    }

    public EmailProperties getEmailProperties()
    {
        return (EmailProperties) this.properties.get("Email");
    }
}
