package Base;

import java.io.*;
import java.util.Random;


public class Person {
    /*
    To jest główna klasa, która zawiera wszytskie dane o konkretnej stworzonej osobie.
    Ma też metody do zmiany konkretnych danych i wypisywania danych o osobie na kolsole
     */

    String name;
    String surname;
    int birth_day,birth_month,birth_year;
    Boolean sex;
    String pesel;
    String eye_Colour;
    String[] col_enames = new String[]{"Green","Blue","Gray","Amber","Brown","Hazel","Red"};
    String hair_Style;
    String[] col_style = new String[]{"Long","Middle","Short"};

    public Person() throws IOException {
        Random random = new Random();
        if (random.nextInt(2)==1) {
            sex = true;

            File datafs = new File("C:\\Users\\lukas\\Desktop\\DataFS.csv");//Import plików
            BufferedReader readfs = new BufferedReader(new FileReader(datafs));//Czytnik pliku z którego korzystam
            String currentlinefs = readfs.readLine();//1 linia to podpisy wiec ich nie potrzebujemy
            String[] listfs = new String[1];//tablica wielkosci 2
            //losujemy liczbe i dopóki nie przekroczymy takiej ilości imion idziemy z nimi dalej
            for (int i = random.nextInt(25681428); i > 0; i = i - Integer.parseInt(listfs[1])) {
                currentlinefs = readfs.readLine();
                listfs = currentlinefs.split(",");
            }
            this.surname = listfs[0];

            //Analogicznie co wyżej
            File datafn = new File("C:\\Users\\lukas\\Desktop\\DataFN.csv");//Import plików
            BufferedReader readfn = new BufferedReader(new FileReader(datafn));//Czytnik pliku z którego korzystam
            String currentlinefn = readfn.readLine();
            String[] listfn = new String[1];
            for (int i = random.nextInt(25681428); i > 0; i = i - Integer.parseInt(listfn[1])) {
                currentlinefn = readfn.readLine();
                listfn = currentlinefn.split(",");
            }
            this.name = listfn[0];

        }else {
            //Analogicznie co wyżej
            sex = false;

            File datams = new File("C:\\Users\\lukas\\Desktop\\DataMS.csv");//Import plików
            BufferedReader readms = new BufferedReader(new FileReader(datams));//Czytnik pliku z którego korzystam
            String currentlinems = readms.readLine();
            String[] listms = new String[1];
            for (int i = random.nextInt(25681428); i > 0; i = i - Integer.parseInt(listms[1])) {
                currentlinems = readms.readLine();
                listms = currentlinems.split(",");
            }
            this.surname = listms[0];

            //Analogicznie co wyżej
            File datamn = new File("C:\\Users\\lukas\\Desktop\\DataMN.csv");//Import plików
            BufferedReader readmn = new BufferedReader(new FileReader(datamn));//Czytnik pliku z którego korzystam
            String currentlinemn = readmn.readLine();
            String[] listmn = new String[1];
            for (int i = random.nextInt(25681428); i > 0; i = i - Integer.parseInt(listmn[1])) {
                currentlinemn = readmn.readLine();
                listmn = currentlinemn.split(",");
            }
            this.name = listmn[0];
        }

        this.birth_year = 2020-random.nextInt(70); //losowanie roku urodzenia
        this.birth_month = 13-random.nextInt(12); //losowanie miesiąca
        //Wybór dnia urodznia.Kolejne przypadki to różne ilości dni w mieśiącu
        if(birth_month==1||birth_month==3||birth_month==5||birth_month==7||birth_month==8||birth_month==10||birth_month==12){
            this.birth_day = 32-random.nextInt(31);
        }else{
            this.birth_day = 31-random.nextInt(30);
        }
        if(birth_month==2){
            if((birth_year%4==0 && birth_year%100!=0)||birth_year%400==0){
                this.birth_day = 30 - random.nextInt(29);
            }else{
                this.birth_day = 29 - random.nextInt(28);
            }
        }
        //Pierwsze 2 liczby w peselu odpowiadają za rok urodzenia są trzy przypadki
        if (birth_year%100<10){
            if (birth_year==2000){
                pesel = "00";
            }else {
                pesel = "0"+ Integer.toString(birth_year%100);
            }
        }else {
            pesel = Integer.toString(birth_year % 100);
        }
        if(birth_year<1999){
            if (birth_month<10) { //gdy miesiąc nie jest 10,11,12 to trzeba dać 0 przed by było np: 05 czy 01
                pesel = pesel +"0"+Integer.toString(birth_month);
            } else {
                pesel = pesel +Integer.toString(birth_month);
            }
        } else {
            pesel = pesel + Integer.toString(birth_month + 20);
        }
        if (birth_day>9){ // gdy jest tylko jedna cyfra należy dodać zero przed nią
            pesel = pesel + Integer.toString(birth_day);
        }else {
            pesel = pesel + "0" + Integer.toString(birth_day);
        }
        // dodanie 3 losowych cyfr po dacie urodzenia
        pesel = pesel + Integer.toString(10 - random.nextInt(10)) + Integer.toString(10 - random.nextInt(10))+ Integer.toString(10 - random.nextInt(10));
        if (sex){ //przed ostatnia cyfra zależy tylko od płci
            pesel = pesel + Integer.toString((4-random.nextInt(4))*2);
        }else {
            pesel = pesel + Integer.toString((5- random.nextInt(5))*2 + 1);
        }
        String[] pesel_chek = pesel.split(""); //Ostatnie cyfra ma specjaleną właściwośc i da się ją wygenerować za pomocą reszty liczb
        int sum=0;//nasza suma poprzednich cyfr peselu.Od niej zależy ostania
        int[] numb = new int[]{9,7,3,1};//Współczynniki do wymnożenia
        for (int j=0;j<10;j++){
            sum = sum + Integer.parseInt(pesel_chek[j])*numb[j%4];
        }
        pesel = pesel + sum%10;
        this.pesel = pesel;

        int col_picker = random.nextInt(col_enames.length+1);
        this.eye_Colour = col_enames[col_picker];

        col_picker = random.nextInt(col_style.length +1);
        this.hair_Style = col_style[col_picker];




    }
    public void setEye_Colour(String colour){
        //wybór powinnien być z listy wybranych kolorów(chyba może ręczne wpisywanie?)
        this.eye_Colour = colour;
    }
    public void setHair_Style(String style){
        this.hair_Style = style;
    }



}
