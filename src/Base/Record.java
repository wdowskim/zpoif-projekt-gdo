package Base;

import Attributes.Attribute;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Record {
    /*
    Klasa na pojedyńczy rekord z docelowej tabeli.
    Zawiera wartości atrybutów (kolumn) - dziedziczne z klasy Attribute.
     */

    // attributes - mapa z wartościami każdego atrybutu
    private Map<String, Attribute> attributes; // to musi byc LinkedHashMap!!!
    // wholeRecord - lista z tekstowym przedstawieniem wartosci atrybutów
    private List<String> wholeRecord;
    // selectedRecord - lista z wybranymi wartosciami atrybotow
    private List<String> selectedRecord = new ArrayList<>();

    // attributeNames - lista na tekstowe tytuły kolumn
    private List<String> attributeNames;
    // selectedAttributeNames - lista na tekstowe tytuły kolumn
    private List<String> selectedAttributeNames = new ArrayList<>();



    public Record(Map<String, Attribute> entry)
    {
        // konstruktor otrzymuje mapę z utworzonymi wcześniej, na podstawie wejscia z GUI, atrybutami i przekazuje nazwy
        this.attributes = entry;
        this.attributeNames = new ArrayList<String>(this.attributes.keySet());
        // stworzenie selectedAttributeNames
        for (String attr : this.attributeNames)
        {
            if (Global.CHOSEN_ATTRIBUTES.get(attr))
            {
                this.selectedAttributeNames.add(attr);
            }
        }

        // zainicjowanie listy na tekstowe przedstawienie wartosci
        this.wholeRecord = new ArrayList<>();
    }

    // generateOne tworzy jeden losowy rekord
    private void generateOne() throws IOException {
        // wyczyszczenie listy na początku generowania nowego rekordu
        this.wholeRecord.clear();
        this.selectedRecord.clear();

        // metoda generate generuje dane z atrybutów i zapisuje do listy record
        for (Map.Entry<String, Attribute> attr : this.attributes.entrySet())
        {
            // wygenerowanie wartosci atrybutu na podstawie pozostałych danych z rekordu
            attr.getValue().generate(this.attributes);

            // dopisanie wartosci obecnego atrybutu do listy record
            this.wholeRecord.add(attr.getValue().toString());

            // dodanie atrybutu z listy wybranych atrybutów
            if (Global.CHOSEN_ATTRIBUTES.get(attr.getKey()))
            {
                this.selectedRecord.add(attr.getValue().toString());
            }
        }
    }

    // writeOne wypisuje obecny stan listy record
    private String writeOne()
    {
        // pusty napis na wynikowe dane
        // TODO: w domyśle mają być rożne dla różnych typów danych
        String output = "";

        for (String fields : this.wholeRecord)
        {
            // dopisanie pola w odpowiedni dla wynikowych danych sposob
            output += fields + "\t";
        }
        // zakonczenie znakiem nowej linii
        output += "\n";

        return output;
    }

    // generate generuje napis (później pewnie plik) z wygenerowanymi danymi
    public String generate(int quantity) throws IOException {
        // pusty napis na wynikowe dane
        // TODO: w domyśle mają być rożne dla różnych typów danych
        String output = "";

        // wykonanie się określoną liczbę razy
        // TODO: być może da się to zrobić szybciej, np strumieniami
        for (int i = 0; i < quantity; i++)
        {
            this.generateOne();
            output += this.writeOne();
        }

        return output;
    }

    public void generateToFile(int quantity, @NotNull String fileType, String filePath) throws Exception
    {
        // zainicjowanie zmiennych z elementami składni danego formatu
        String fileBeginning = "";
        String fileEnding = "";
        String lineBeginning = "";
        String lineEnding = "";
        switch (fileType)
        {
            case "csv":
                fileBeginning = String.join(",", this.selectedAttributeNames) + "\n";
                fileEnding = "";
                lineBeginning = "";
                lineEnding = "\n";
                break;
            case "tsv":
                fileBeginning = String.join("\t", this.selectedAttributeNames) + "\n";
                fileEnding = "";
                lineBeginning = "";
                lineEnding = "\n";
                break;
            case "json":
                fileBeginning = "[";
                fileEnding = "]";
                lineBeginning = "";
                lineEnding = ",\n";
                break;
            case "xml":
                fileBeginning = "<records> + \"\\n\"";
                fileEnding = "</records>";
                lineBeginning = "";
                lineEnding = "";
                break;
        }

        // zainicjowanie zapisywania do pliku
        BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
        String row = "";

        // linia początkowa
        row = fileBeginning;
        writer.write(row);

        // zapisywanie dla wszytskich oprócz ostatniego
        for (int i = 0; i < quantity - 1; i++)
        {
            this.generateOne();
            row = lineBeginning + formatRecordAs(fileType) + lineEnding;
            writer.write(row);
        }
        // zapisywanie ostatniego
        this.generateOne();
        row = lineBeginning + formatRecordAs(fileType) + fileEnding;
        writer.write(row);

        // zakończenie zapisywania
        writer.close();
    }

    // Metoda zwraca sformatowaną do odpowiedniego typu danych linię tekstu - rekord
    private String formatRecordAs(@NotNull String fileType) throws Exception {
        String output = "";

        switch (fileType)
        {
            case "csv":
                output = String.join(",", this.selectedRecord);
                break;
            case "tsv":
                output = String.join("\t", this.selectedRecord);
                break;
            case "json":
                output += "{";
                // pierwsze elementy
                for (int i = 0; i < this.wholeRecord.size() -1; i++)
                {
                    output += "\"" + this.selectedAttributeNames.get(i) + "\":\"" + this.wholeRecord.get(i) + "\",";
                }
                // ostatni element
                output += "\"" + this.selectedAttributeNames.get(this.wholeRecord.size()-1) + "\":\"" + this.wholeRecord.get(this.wholeRecord.size()-1) + "\"";
                output += "}";
                break;
            case "xml":
                output += "<Record>";
                for (int i = 0; i < this.wholeRecord.size(); i++)
                {
                    output += "<" + this.selectedAttributeNames.get(i) + ">" + this.wholeRecord.get(i) + "</" + this.selectedAttributeNames.get(i) + ">";
                }
                output += "</Record>";
                break;
            default:
                throw new Exception("Nieznany format pliku");
        }

        return output;
    }
}
