package Base;

import java.io.*;
import java.util.TreeMap;

public class ProbabilityFromFile {
    /*
    Klasa ProbabilityFromFile służy do stworzenia z danych z pliku mapy, która okresli prawdopodobienstwo wystapienia danego wyniku.
     */

    private TreeMap<Double, String> probability;
    private Double divider;

    public ProbabilityFromFile(String filepath) throws IOException {

        this.probability = new TreeMap<Double, String>();

        // zczytanie pliku z folderu resources (przekazanym w argumencie filepath)
        File fn = new File(filepath); // "resources/DataFN.csv"
        BufferedReader readFn = new BufferedReader(new FileReader(fn));

        // zainicjowanie linii na wejście
        String currentLineFn;

        // zainicjowanie zmiennych pomocniczych
        double quantity = 0.0; // do sumowania liczby wszystkich wystąpień
        double currentQuantity; // do aktualnej liczby wystąpień
        String[] currentLineEntries; // do podziału linii
        String currentName; // do aktualnego wyniku
        boolean entered = false; // do zignorowania nagłówka pliku

        while ((currentLineFn = readFn.readLine()) != null)
        {
            // zignorowanie pierwszej linii - nagłówka pliku
            // TODO: poprawić tak, żeby to się sprawdzało raz - najlepiej wywalić za pętlę
            if (!entered)
            {
                entered = true;
                continue;
            }

            //debug
            //System.out.println(currentLineFn);

            // podział linii
            currentLineEntries = currentLineFn.split(",");

            // przypisanie odpowiednich wartości
            currentName = currentLineEntries[0];
            currentQuantity = Double.parseDouble(currentLineEntries[1]);
            quantity += currentQuantity;

            // dopisanie do mapy
            this.probability.put(quantity, currentName);
        }

        this.divider = quantity; // końcowa liczba wszystkich elementów
    }

    public String generate()
    {
        // stworzenie losowej wartości
        Double r = Math.random()*this.divider;
        // wybranie pierwszego elementu o wartości większej od wylosowanej - w taki sposób dobieramy imię wg. rozkładu
        String output = this.probability.get(this.probability.higherKey(r));

        // zapewnienie że to nie będzie null
        while (output == null)
        {
            output = this.probability.get(this.probability.higherKey(r));
        }

        return output;
    }
}
