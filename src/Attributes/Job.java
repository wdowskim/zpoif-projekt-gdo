package Attributes;

import Base.Global;

import java.util.Map;

public class Job extends Attribute {
    String value;
    double probJob;

    public Job(){
        this.probJob = Global.CURRENT_PROPERTIES.getJobPropeties().probJob;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        int y = Integer.parseInt(String.valueOf(birthYear));
        if(y>2002 || y < 1955){
            this.value = "Nie Pracuje"; //Nazwa by było wiadomo że w złym wieku
        }
        else if(Math.random()<probJob){
            this.value = "Pracuje";
        }
        else {this.value = "Bezrobotny";}
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
