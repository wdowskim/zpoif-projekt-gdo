package Attributes;

import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.IOException;
import java.util.Calendar;
import java.util.Map;
import java.util.Random;

public class BirthYear extends Attribute {

    private String value;

    private ProbabilityFromFile femaleBirthYear;
    private ProbabilityFromFile maleBirthYear;

    public BirthYear(String femaleFilePath, String maleFilePath) throws IOException
    {
        this.femaleBirthYear = new ProbabilityFromFile(femaleFilePath);
        this.maleBirthYear = new ProbabilityFromFile(maleFilePath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        Sex sex = (Sex) attributes.get("Sex");
        Random random = new Random(); //Do lowoswania intów
        Calendar cal = Calendar.getInstance();
        if (sex.isMale()){
            this.value = maleBirthYear.generate();
        }
        else if (sex.isFemale()){
            this.value = femaleBirthYear.generate();
        }
        else {
            {
                if (Probability.happensWithProbability(0.5)) this.value = this.maleBirthYear.generate();
                else this.value = this.femaleBirthYear.generate();
            }
        }
        this.value = Integer.toString(cal.get(Calendar.YEAR) - Integer.parseInt(value) - random.nextInt(4)); //Wiek jest co 5 lat a więc losuje równomiernie dodanie 0/1/2/3/4 lat do wieku
    }

    @Override
    public String toString()
    {
        //PO CO? - Marcin //Już wiem XD - marcin
        return this.value;
    }

    @Override
    public Object getValue()
    {
        return this.value;
    }
}
