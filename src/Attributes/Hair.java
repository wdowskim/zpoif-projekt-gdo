package Attributes;

import Base.Global;

import java.util.Calendar;
import java.util.Map;

public class Hair extends Attribute {

    String value;

    double probBrown;
    double probBlack;
    double probBlond;
    double probAuburn;
    double probRed;
    boolean allowGrey;

    public Hair(){
        this.probAuburn = Global.CURRENT_PROPERTIES.getHairProperties().probAuburn;
        this.probBlack = Global.CURRENT_PROPERTIES.getHairProperties().probBlack;
        this.probBrown = Global.CURRENT_PROPERTIES.getHairProperties().probBrown;
        this.probBlond = Global.CURRENT_PROPERTIES.getHairProperties().probBlond;
        this.probRed = Global.CURRENT_PROPERTIES.getHairProperties().probRed;
        this.allowGrey = Global.CURRENT_PROPERTIES.getHairProperties().allowGray;
    }
    @Override
    public void generate(Map<String, Attribute> attributes) {
        BirthMonth birthMonth = (BirthMonth) attributes.get("BirthMonth");
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        BirthDay birthDay = (BirthDay) attributes.get("BirthDay");
        Calendar cal = Calendar.getInstance();
        int d = (int) birthDay.getValue();
        int m = birthMonth.getNumeric();
        int y = Integer.parseInt(String.valueOf(birthYear));
        double randomValue = Math.random()*(probRed+probBlond+probBrown+probAuburn+probBlack);
        if(randomValue<probRed){
            this.value = "Rude";
        }
        else if (randomValue<probRed+probBlond){
            this.value = "Blond";
        }
        else if (randomValue<probRed+probBlond+probBrown){
            this.value = "Brązowe";
        }
        else if (randomValue<probRed+probBlond+probBrown+probAuburn){
            this.value = "Kasztanowe";
        }
        else{
            this.value = "Czarne";
        }
        if(allowGrey && Math.random()> (cal.get(Calendar.YEAR)-y)/80.0 && cal.get(Calendar.YEAR)-y > 25){
            this.value="Siwe";
        }
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
