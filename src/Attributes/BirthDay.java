package Attributes;

import java.util.Map;
import java.util.Random;

public class BirthDay extends Attribute {

    private int value;

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        BirthMonth birthMonth = (BirthMonth) attributes.get("BirthMonth");
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        Random random = new Random();
        int mh = birthMonth.getNumeric();
        if(mh==1||mh==3||mh==5||mh==7||mh==8||mh==10||mh==12){
            this.value = 31-random.nextInt(31);
        }
        else if(mh==4||mh==6||mh==9||mh==11){
            this.value = 30-random.nextInt(30);
        }
        else {
            int yh = Integer.parseInt(birthYear.toString());
            if((yh%4==0&&yh%100!=0)||yh%400==0){
                this.value = 29-random.nextInt(29);
            }
            else {
                this.value = 28-random.nextInt(28);
            }


        }
    }

    @Override
    public String toString()
    {
        return Integer.toString(this.value);
    }

    @Override
    public Object getValue()
    {
        return this.value;
    }
}
