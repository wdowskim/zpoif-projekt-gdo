package Attributes;

import Base.Global;

import java.util.Map;

public class Education extends Attribute {

    String value;
    double probPodst;
    double probGimn;
    double probZasa;
    double probZaBr;
    double probSrBr;
    double probsred;
    double probWyzs;

    public Education(){
        this.probPodst = Global.CURRENT_PROPERTIES.getEducationProperties().probPodst;
        this.probGimn = Global.CURRENT_PROPERTIES.getEducationProperties().probGimn;
        this.probZasa = Global.CURRENT_PROPERTIES.getEducationProperties().probZasa;
        this.probZaBr = Global.CURRENT_PROPERTIES.getEducationProperties().probZaBr;
        this.probSrBr = Global.CURRENT_PROPERTIES.getEducationProperties().probSrBr;
        this.probsred = Global.CURRENT_PROPERTIES.getEducationProperties().probsred;
        this.probWyzs = Global.CURRENT_PROPERTIES.getEducationProperties().probWyzs;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        double randomValue = Math.random()*(probPodst+probGimn+probZasa+probZaBr+probSrBr+ probsred+probWyzs);
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        int y = Integer.parseInt(String.valueOf(birthYear));
        if(randomValue<probPodst || y > 2007){
            this.value = "Podstatowe";
        }
        else if (randomValue<probPodst+probGimn || y > 2004){
            this.value = "Gimnazjale";
        }
        else if (randomValue<probPodst+probGimn+probZasa){
            this.value = "Zasadnicze";
        }
        else if (randomValue<probPodst+probGimn+probZasa+probZaBr){
            this.value = "Zasadniczo-Branżowe";
        }
        else if (randomValue<probPodst+probGimn+probZasa+probZaBr+probSrBr){
            this.value = "Średnio-Branżowe";
        }
        else if(randomValue<probPodst+probGimn+probZasa+probZaBr+probSrBr+probsred){
            this.value = "Średnie";
        }
        else {this.value = "Wyższe";}
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
