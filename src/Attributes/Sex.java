package Attributes;

import Base.Global;
import Base.Probability;

import java.util.Map;

public class Sex extends Attribute {
    /*
    Klasa generująca płeć biologiczną
     */

    private String value;

    public static String maleSymbol;
    public static String femaleSymbol;

    private double maleProbability;

    /*
    TAK BYŁO PRZED:

    public Sex( double maleProbability, String maleSymbol, String femaleSymbol)
    {
        Sex.maleSymbol = maleSymbol;
        Sex.femaleSymbol = femaleSymbol;
        this.maleProbability = maleProbability;
    }
    */

    // A TAK MA BYĆ DOCELOWO
    public Sex()
    {
        Sex.maleSymbol = Global.CURRENT_PROPERTIES.getSexProperties().maleSymbol;
        Sex.femaleSymbol = Global.CURRENT_PROPERTIES.getSexProperties().femaleSymbol;
        this.maleProbability = Global.CURRENT_PROPERTIES.getSexProperties().maleFraction;
    }


    // metoda generate generuje losowo wartość atrybutu na podstawie podanych cech i pozostałych danych
    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        if (Probability.happensWithProbability(this.maleProbability))
        {
            this.value = Sex.maleSymbol;
        }
        else
        {
            this.value = Sex.femaleSymbol;
        }
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }

    public boolean isMale()
    {
        return maleSymbol == this.value;
    }

    public boolean isFemale()
    {
        return femaleSymbol == this.value;
    }
}
