package Attributes;

import java.io.IOException;
import java.util.Map;

public abstract class Attribute {
    /*
    Klasa Attributes jest wzorcem na kolejne atrybuty.
     */

    // value - wartość danego atrybutu
    private Object value;
    // properties - cechy do generatora
    //private Properties properties;

    // metoda generate generuje losowo wartość atrybutu na podstawie podanych cech i pozostałych danych
    public abstract void generate(Map<String, Attribute> attributes) throws IOException;

    // metoda toString - przedstawia sposób wypisania wartości value
    public abstract String toString();

    public abstract Object getValue();
}
