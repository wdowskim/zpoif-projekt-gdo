package Attributes;

import Base.Global;
import Base.Probability;

import java.util.Map;

public class Gender extends Attribute {
    /*
    Klasa generująca płeć społeczną
     */

    private String value;

    // genderDysphoriaProbability - określa, jakie jest prawdopodobieństwo, że u jednej osoby płeć społeczna nie pokrywa się z płcią biologiczną
    private double genderDysphoriaProbability;
    private double otherGenderProbability;

    public static String otherSymbol;
    /*
    public Gender(double genderDysphoriaProbability, double otherGenderProbability, String otherSymbol)
    {
        this.otherGenderProbability = otherGenderProbability;
        this.genderDysphoriaProbability = genderDysphoriaProbability;
        this.otherSymbol = otherSymbol;
    }
    */
    public Gender()
    {
        this.otherGenderProbability = Global.CURRENT_PROPERTIES.getGenderProperties().otherGenderProbability;
        this.genderDysphoriaProbability = Global.CURRENT_PROPERTIES.getGenderProperties().genderDysphoriaProbability;
        this.otherSymbol = Global.CURRENT_PROPERTIES.getGenderProperties().otherSymbol;
    }

    // metoda generate generuje losowo wartość atrybutu na podstawie podanych cech i pozostałych danych
    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        Sex sex = (Sex) attributes.get("Sex");
        if (Probability.happensWithProbability(this.genderDysphoriaProbability))
        {
            if (sex.isMale())
            {
                this.value = Sex.femaleSymbol;
            }
            else
            {
                this.value = Sex.maleSymbol;
            }
        }
        else
        {
            if (Probability.happensWithProbability(this.otherGenderProbability))
            {
                this.value = Gender.otherSymbol;
            }
            else
            {
                this.value = sex.getValue();
            }
        }
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }

    public boolean isMale()
    {
        return Sex.maleSymbol == this.value;
    }

    public boolean isFemale()
    {
        return Sex.femaleSymbol == this.value;
    }

    public boolean isOther()
    {
        return Gender.otherSymbol == this.value;
    }
}
