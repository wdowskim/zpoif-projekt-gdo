package Attributes;

import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class MotherName extends Attribute {
    /*
    Klasa generująca pierwsze imię.
    */

    // value - wartość danego atrybutu
    private String value;

    // maleNames - prawdopodobieństwo z pliku imion mężczyzn
    private ProbabilityFromFile femaleNames;

    public MotherName(String femaleNamesPath) throws IOException
    {
        this.femaleNames = new ProbabilityFromFile(femaleNamesPath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        this.value = this.femaleNames.generate();
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}
