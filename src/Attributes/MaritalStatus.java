package Attributes;

import java.util.Map;

public class MaritalStatus extends Attribute {

    private String value;

    @Override
    public void generate(Map<String, Attribute> attributes) {
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        int y = Integer.parseInt(String.valueOf(birthYear));
        if (y>2002){
            value = "Single";
        }
        else if(y > 1995 ) {
            if(Math.random()>((2003-y)/21)){
                value = "Married";
            }
            else {
                value = "Single";
            }
        }
        else if(y>1985){
            if (Math.random()>((1996-y)/20 + 5/10)){
                value = "Married";
            }
            else {
                value = "Single";
            }
        }
        else {
            if (Math.random()<0.1){
                value = "Single";
            }
            else {
                if(Math.random()>(1 - Math.exp((y-1985)/20))){
                    value = "Widowed";
                }
                else {
                    value = "Married";
                }
            }
        }



    }

    public boolean hasBeenMarried()
    {
        return this.value == "Married" || this.value == "Widowed";
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
