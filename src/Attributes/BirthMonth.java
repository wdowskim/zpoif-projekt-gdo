package Attributes;

import Base.Global;
import Base.Probability;

import java.util.LinkedHashMap;
import java.util.Map;

public class BirthMonth extends Attribute {

    private String value;
    private Integer numeric;

    public static Map<String, String> months;
    public static Map<String, Integer> conv;

    public BirthMonth()
    {
        String januarySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().januarySymbol;
        String februarySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().februarySymbol;
        String marchSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().marchSymbol;
        String aprilSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().aprilSymbol;
        String maySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().maySymbol;
        String juneSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().juneSymbol;
        String julySymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().julySymbol;
        String augustSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().augustSymbol;
        String septemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().septemberSymbol;
        String octoberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().octoberSymbol;
        String novemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().novemberSymbol;
        String decemberSymbol = Global.CURRENT_PROPERTIES.getBirthMonthProperties().decemberSymbol;

        months = new LinkedHashMap<>();
        conv = new LinkedHashMap<>();
        months.put("January", januarySymbol);
        conv.put(januarySymbol,1); //Przy generowaniu dnia urodzin potrzebujemy miesiąca i to łączy
        months.put("February", februarySymbol);//liczbe miesiąca z dowolnie nazwanym miesiącem
        conv.put(februarySymbol,2);
        months.put("March", marchSymbol);
        conv.put(marchSymbol,3);
        months.put("April", aprilSymbol);
        conv.put(aprilSymbol,4);
        months.put("May", maySymbol);
        conv.put(maySymbol,5);
        months.put("June", juneSymbol);
        conv.put(juneSymbol,6);
        months.put("July", julySymbol);
        conv.put(julySymbol,7);
        months.put("August", augustSymbol);
        conv.put(augustSymbol,8);
        months.put("September", septemberSymbol);
        conv.put(septemberSymbol,9);
        months.put("October", octoberSymbol);
        conv.put(octoberSymbol,10);
        months.put("November", novemberSymbol);
        conv.put(novemberSymbol,11);
        months.put("December", decemberSymbol);
        conv.put(decemberSymbol,12);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        this.value = Probability.discreteUniform(months.get("January"), months.get("February"), months.get("March"), months.get("April"), months.get("May"), months.get("June"),
                months.get("July"), months.get("August"), months.get("September"), months.get("October"), months.get("November"), months.get("December"));
        this.numeric = conv.get(this.value);
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public Object getValue()
    {
        return this.value;
    }

    public int getNumeric() {return this.numeric; }

}
