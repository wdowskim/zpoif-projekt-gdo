package Attributes;

import Base.Global;
import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class MaidenName extends Attribute {
    /*
    Klasa generująca nazwisko.
    */

    // value - wartość danego atrybutu
    private String value;

    // femaleSurnames - prawdopodobieństwo z pliku nazwisk kobiet
    private ProbabilityFromFile femaleSurnames;

    public MaidenName(String femaleSurnamesPath) throws IOException
    {
        this.femaleSurnames = new ProbabilityFromFile(femaleSurnamesPath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        // nazwisko rodowe jest dobierane zależnie od płci społecznej i stanu cywilnego
        Gender gender = (Gender) attributes.get("Gender");
        MaritalStatus maritalStatus = (MaritalStatus) attributes.get("MaritalStatus");
        if (gender.isFemale() && maritalStatus.hasBeenMarried())
        {
            this.value = this.femaleSurnames.generate();
        }
        else
        {
            this.value = Global.EMPTY_STRING;
        }
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}