package Attributes;

import Base.ProbabilityFromFile;

import java.io.IOException;
import java.util.Map;

public class State extends Attribute {
    String value;

    private ProbabilityFromFile city;

    public State(String path) throws IOException {
        city = new ProbabilityFromFile(path);
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        this.value = city.generate();
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
