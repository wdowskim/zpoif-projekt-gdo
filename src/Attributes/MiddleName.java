package Attributes;

import Base.Global;
import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class MiddleName extends Attribute {
    /*
    Klasa generująca drugie imię.
    */

    // value - wartość danego atrybutu
    private String value;
    // femaleNames - prawdopodobieństwo z pliku imion kobiet
    private ProbabilityFromFile femaleNames;
    // maleNames - prawdopodobieństwo z pliku imion mężczyzn
    private ProbabilityFromFile maleNames;
    // middleNameProbability - prawdopodobieństwo, że osoba ma drugie imię
    private double middleNameProbability;

    public MiddleName(String femaleNamesPath, String maleNamesPath) throws IOException
    {
        this.femaleNames = new ProbabilityFromFile(femaleNamesPath);
        this.maleNames = new ProbabilityFromFile(maleNamesPath);
        this.middleNameProbability = Global.CURRENT_PROPERTIES.getMiddleNameProperties().middleNameFraction;
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        // drugie imię jest dobierane zależnie od płci społęcznej
        Gender gender = (Gender) attributes.get("Gender");
        // losujemy raz, następnie upewniamy się, że drugie imię jest różne od pierwszego
        do {
            // określamy, czy dana osoba ma drugie imię - jeśli nie, to wychodzimy z pętli i przypisujemy wartość
            // przypisana wartość jest odpowiednia dla braku danych dla napisów - określona w klasie Global
            if (Math.random() >= this.middleNameProbability)
            {
                this.value = Global.EMPTY_STRING;
                break;
            }
            if (gender.isMale())
            {
                this.value = this.maleNames.generate();
            }
            if (gender.isFemale())
            {
                this.value = this.femaleNames.generate();
            }
            // jeśli jest płeć "other", to losujemy pulę z której weźmiemy imię
            if (gender.isOther())
            {
                if (Probability.happensWithProbability(0.5)) this.value = this.maleNames.generate();
                else this.value = femaleNames.generate();
            }
        }
        while (attributes.get("FirstName").getValue() == this.value);
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}
