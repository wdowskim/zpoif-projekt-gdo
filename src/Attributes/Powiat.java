package Attributes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Powiat extends Attribute {

    String value;
    String filepath;

    @Override
    public void generate(Map<String, Attribute> attributes) throws IOException {
        State state = (State) attributes.get("State");
        filepath = "resources/" + (String) state.getValue() + ".csv";
        File fn = new File(filepath);
        BufferedReader readFn = new BufferedReader(new FileReader(fn));
        String currentLineFn;
        String[] currentLineEntries;
        List<String> names = new ArrayList<>();
        List<Integer> cumsum = new ArrayList<>();
        int suma=0;
        while ((currentLineFn = readFn.readLine()) != null){
            currentLineEntries = currentLineFn.split(",");
            if(currentLineEntries.length>4 && currentLineEntries[0].length()>2){
                String firstword = currentLineEntries[0];
                char checer = firstword.charAt(0);
                String test = "p";
                char te = test.charAt(0);
                if(te == checer){
                    suma = suma + Integer.parseInt(currentLineEntries[2]);
                    names.add(firstword);
                    cumsum.add(suma);
                }
            }
        }
        double randvalue = Math.random()*suma;
        for (int i=0;i<names.size();i++){
            this.value = names.get(i);
            if(cumsum.get(i)>randvalue){
                break;
            }
        }
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
