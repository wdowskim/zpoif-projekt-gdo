package Attributes;

import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class Surname extends Attribute {
    /*
    Klasa generująca nazwisko.
    */

    // value - wartość danego atrybutu
    private String value;

    // femaleSurnames - prawdopodobieństwo z pliku nazwisk kobiet
    private ProbabilityFromFile femaleSurnames;
    // maleSurnames - prawdopodobieństwo z pliku nazwisk mężczyzn
    private ProbabilityFromFile maleSurnames;

    public Surname(String femaleSurnamesPath, String maleSurnamesPath) throws IOException
    {
        this.femaleSurnames = new ProbabilityFromFile(femaleSurnamesPath);
        this.maleSurnames = new ProbabilityFromFile(maleSurnamesPath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        // nazwisko jest dobierane zależnie od płci społecznej
        Gender gender = (Gender) attributes.get("Gender");
        if (gender.isMale())
        {
            this.value = this.maleSurnames.generate();
        }
        if (gender.isFemale())
        {
            this.value = this.femaleSurnames.generate();
        }
        // jeśli jest płeć "other", to losujemy pulę z której weźmiemy nazwisko
        if (gender.isOther())
        {
            if (Probability.happensWithProbability(0.5)) this.value = this.maleSurnames.generate();
            else this.value = this.femaleSurnames.generate();
        }
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}