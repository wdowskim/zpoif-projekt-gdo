package Attributes;

import java.util.Map;
import java.util.Random;

public class Pesel extends Attribute {
    public String value;
    @Override
    public void generate(Map<String, Attribute> attributes) {
        value = "";
        Random random = new Random();
        Sex sex = (Sex) attributes.get("Sex");
        BirthMonth birthMonth = (BirthMonth) attributes.get("BirthMonth");
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        BirthDay birthDay = (BirthDay) attributes.get("BirthDay");
        int d = (int) birthDay.getValue();
        int m = birthMonth.getNumeric();
        int y = Integer.parseInt(String.valueOf(birthYear)); //Tak to chyba nie ma działać XD

        if(y==2000){
            value+="00";
        }else if (y%100<10){
            value+="0"+y%100;
        }else {
            value+=y%100;
        }
        if (y<2000){
            if(m<10){
                value+="0"+m;
            }
            else {
                value+=m;
            }
        }else {
            if(m<10){
                value+="2"+m;
            }
            else {
                value+=Integer.toString(m+20);
            }
        }
        if(d<10){
            value+="0"+d;
        }
        else {
            value+=d;
        }
        value+=random.nextInt(9);
        value+=random.nextInt(9);
        value+=random.nextInt(9);
        if (sex.isMale()){
            value+=2*random.nextInt(4)+1;
        }else { //Co z 3 płcią???????
            value += 2 * random.nextInt(4);
        }
        int sum=0;
        String[] peselCheck = value.split("");
        int[] numb = new int[]{9,7,3,1};//Współczynniki do wymnożenia
        for(int j=0;j<10;j++){
            sum+=Integer.parseInt(peselCheck[j])*numb[j%4];
        }
        value+=sum%10;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
