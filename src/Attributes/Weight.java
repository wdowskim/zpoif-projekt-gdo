package Attributes;

import Base.Global;

import java.util.Map;
import java.util.Random;

public class Weight extends Attribute {

    private String value;

    public double underProbability;
    public double normalProbability;
    public double overProbability;

    public Weight(){
        this.underProbability = Global.CURRENT_PROPERTIES.getWeightProperties().probUnder;
        this.normalProbability = Global.CURRENT_PROPERTIES.getWeightProperties().probNorm;
        this.overProbability = Global.CURRENT_PROPERTIES.getWeightProperties().probOver;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {

        Height height = (Height) attributes.get("Height");
        Random random = new Random();
        double h = Double.parseDouble(String.valueOf(height));
        double mass;


        double randomValue = Math.random()*(normalProbability+underProbability+overProbability);

        if (randomValue > this.normalProbability + this.underProbability) {
            mass = 30*h*h*(1 - random.nextGaussian()/10);
        }
        else if (randomValue < this.underProbability) {
            mass = 18*h*h*(1 - random.nextGaussian()/10);
        }
        else {
            mass = 22*h*h*(1-random.nextGaussian()/10);
        }

        value = Double.toString(Math.floor(mass*100)/100);



    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
