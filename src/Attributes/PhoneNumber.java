package Attributes;

import Base.Global;

import java.util.Map;
import java.util.Random;

public class PhoneNumber extends Attribute {

    String value;
    Double prob;

    public PhoneNumber(){
        this.prob = Global.CURRENT_PROPERTIES.getPhoneNumberProperties().phoneNumberFraction;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        Random random = new Random();
        if (Math.random()>prob){
            value = Global.EMPTY_STRING;
        }
        else {
            value = "";
            for (int i = 0; i < 9; i++) {
                value = value + random.nextInt(10);

            }
        }
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
