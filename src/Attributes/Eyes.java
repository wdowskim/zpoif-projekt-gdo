package Attributes;

import Base.Global;

import java.util.Map;

public class Eyes extends Attribute {

    String value;
    double probAmber;
    double probBlue;
    double probBrown;
    double probGrey;
    double probGreen;
    double probHazel;

    public Eyes(){
        this.probHazel = Global.CURRENT_PROPERTIES.getEyesProperties().probHazel;
        this.probGrey = Global.CURRENT_PROPERTIES.getEyesProperties().probGrey;
        this.probGreen = Global.CURRENT_PROPERTIES.getEyesProperties().probGreen;
        this.probBrown = Global.CURRENT_PROPERTIES.getEyesProperties().probBrown;
        this.probBlue = Global.CURRENT_PROPERTIES.getEyesProperties().probBlue;
        this.probAmber = Global.CURRENT_PROPERTIES.getEyesProperties().probAmber;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        double randomValue = Math.random()*(probAmber+probBlue+probBrown+probGreen+probGrey+ probHazel);
        if(randomValue<probAmber){
            this.value = "Amber";
        }
        else if (randomValue<probAmber+probBlue){
            this.value = "Blue";
        }
        else if (randomValue<probAmber+probBlue+probBrown){
            this.value = "Brown";
        }
        else if (randomValue<probAmber+probBlue+probBrown+probGreen){
            this.value = "Green";
        }
        else if (randomValue<probAmber+probBlue+probBrown+probGreen+probGrey){
            this.value = "Grey";
        }
        else {this.value = "Hazle";}
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
