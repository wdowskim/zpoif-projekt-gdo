package Attributes;

import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class FirstName extends Attribute {
    /*
    Klasa generująca pierwsze imię.
    */

    // value - wartość danego atrybutu
    private String value;

    // femaleNames - prawdopodobieństwo z pliku imion kobiet
    private ProbabilityFromFile femaleNames;
    // maleNames - prawdopodobieństwo z pliku imion mężczyzn
    private ProbabilityFromFile maleNames;

    public FirstName(String femaleNamesPath, String maleNamesPath) throws IOException
    {
        this.femaleNames = new ProbabilityFromFile(femaleNamesPath);
        this.maleNames = new ProbabilityFromFile(maleNamesPath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        // imię jest dobierane zależnie od płci społecznej
        Gender gender = (Gender) attributes.get("Gender");

        if (gender.isMale())
        {
            this.value = this.maleNames.generate();

        }
        if (gender.isFemale())
        {
            this.value = this.femaleNames.generate();
        }
        // jeśli jest płeć "other", to losujemy pulę z której weźmiemy imię
        if (gender.isOther())
        {
            if (Probability.happensWithProbability(0.5)) this.value = this.maleNames.generate();
            else this.value = this.femaleNames.generate();
        }
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}
