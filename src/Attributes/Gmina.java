package Attributes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Gmina extends Attribute {
    String value;
    String filepath;


    @Override
    public void generate(Map<String, Attribute> attributes) throws IOException {
        State state = (State) attributes.get("State");
        Powiat powiat = (Powiat) attributes.get("Powiat");
        filepath = "resources/" + (String) state.getValue() + ".csv";
        String pow = (String) powiat.getValue();
        File fn = new File(filepath);
        BufferedReader readFn = new BufferedReader(new FileReader(fn));
        String currentLineFn;
        String[] currentLineEntries;
        List<String> names = new ArrayList<>();
        List<Integer> cumsum = new ArrayList<>();
        int suma=0;
        char var = ',';
        while ((currentLineFn = readFn.readLine()) != null){
            currentLineEntries = currentLineFn.split(",");
            if(currentLineEntries.length>4 && currentLineEntries[0].length()>2){
                String firstword = currentLineEntries[0];
                char checer = firstword.charAt(0);
                char te = 'p';
                if(firstword.equals(pow)){
                    while ((currentLineFn = readFn.readLine()) != null){
                        if(currentLineFn.charAt(0)==var){
                            break;
                        }
                        currentLineEntries = currentLineFn.split(",");
                        if(currentLineEntries[0]!="") {
                            if (currentLineEntries[0].charAt(0) != te) {
                                firstword = currentLineEntries[0];
                                checer = firstword.charAt(0);
                                char test3 = 'M';
                                char test2 = 'G';
                                if (checer == test3 || checer == test2 ) {
                                    suma = suma + Integer.parseInt(currentLineEntries[2]);
                                    names.add(firstword);
                                    cumsum.add(suma);
                                }
                            }
                        }
                    }
                }
            }
        }
        double randvalue = Math.random()*suma;
        for (int i=0;i<names.size();i++){
            this.value = names.get(i);
            if(cumsum.get(i)>randvalue){
                break;
            }
        }
        this.value = value.split("\\.")[1];
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
