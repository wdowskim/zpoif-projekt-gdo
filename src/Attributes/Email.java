package Attributes;

import Base.Global;
import Base.Probability;

import java.util.Map;
import java.util.Random;

public class Email extends Attribute {

    private String value;
    private Double prob;
    private Random random = new Random();

    public Email(){
        this.prob = Global.CURRENT_PROPERTIES.getEmailProperties().emailFraction;
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        if (!Probability.happensWithProbability(this.prob))
        {
            value = Global.EMPTY_STRING;
        }
        else
        {
            value = "";
            if (Probability.happensWithProbability(0.9))
            {
                value += attributes.get("FirstName").getValue().toString();
            }
            else
            {
                value += attributes.get("FirstName").getValue().toString().charAt(0);
            }

            if (Probability.happensWithProbability(0.5))
            {
                value += ".";
            }

            if (Probability.happensWithProbability(0.6))
            {
                value += attributes.get("Surname").getValue().toString();
            }
            else
            {
                value += attributes.get("Surname").getValue().toString().charAt(0);
            }

            value += Integer.toString(random.nextInt(1000));

            value += "@";

            value += Probability.discreteUniform("gmail.com", "onet.pl", "wp.pl", "op.pl", "o2.pl", "hotmail.com", "t.pl", "interia.pl", "int.pl");

            value = value.toLowerCase();
        }
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
