package Attributes;

import Base.Global;

import java.util.Map;

public class Orientation extends Attribute {
    String value;

    double probHetero;
    double probGay;
    double probBi;
    double probAce;

    public Orientation(){
        this.probAce = Global.CURRENT_PROPERTIES.getOrientationProperties().probAce;
        this.probBi = Global.CURRENT_PROPERTIES.getOrientationProperties().probBi;
        this.probGay = Global.CURRENT_PROPERTIES.getOrientationProperties().probGay;
        this.probHetero = Global.CURRENT_PROPERTIES.getOrientationProperties().probHetero;

    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        double randomValue = Math.random()*(probBi+probGay+probAce+probHetero);
        if(randomValue<probBi){
            this.value = "Bi";
        }
        else if (randomValue<probBi+probGay){
            this.value = "Gay";
        }
        else if (randomValue<probBi+probGay+probAce){
            this.value = "Ace";
        }
        else{ this.value = "Hetero";}

    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
