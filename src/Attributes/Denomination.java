package Attributes;

import Base.Global;

import java.util.Map;

public class Denomination extends Attribute {
    String value;

    double probChrist;
    double probMuslim;
    double probAteis;
    double probHindu;
    double probJudai;

    public Denomination(){
        this.probAteis = Global.CURRENT_PROPERTIES.getDenominationProperties().probAteis;
        this.probChrist = Global.CURRENT_PROPERTIES.getDenominationProperties().probChrist;
        this.probHindu = Global.CURRENT_PROPERTIES.getDenominationProperties().probHindu;
        this.probJudai = Global.CURRENT_PROPERTIES.getDenominationProperties().probJudai;
        this.probMuslim = Global.CURRENT_PROPERTIES.getDenominationProperties().probMuslim;
    }

    @Override
    public void generate(Map<String, Attribute> attributes) {
        double randomValue = Math.random()*(probJudai+probHindu+probMuslim+probChrist+probAteis);
        if(randomValue<probJudai){
            this.value = "Judaism";
        }
        else if (randomValue<probJudai+probHindu){
            this.value = "Hinduism";
        }
        else if (randomValue<probJudai+probHindu+probMuslim){
            this.value = "Islam";
        }
        else if (randomValue<probJudai+probHindu+probMuslim+probChrist){
            this.value = "Christianity";
        }
        else{ this.value = "Ateism";}
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
