package Attributes;

import Base.Global;

import java.util.Map;
import java.util.Random;

public class NrDowodu extends Attribute {
    private String value;
    @Override
    public void generate(Map<String, Attribute> attributes) {
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        int y = Integer.parseInt(String.valueOf(birthYear));
        this.value="";
        if (y>2002){
            this.value = Global.EMPTY_STRING;
        }
        else {
            Random random = new Random();
            int[] beg = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
            String aplha = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int[] numb = new int[]{7, 3, 1, 9, 7, 3, 1, 7, 3};
            beg[0] = (10 + random.nextInt(3));
            beg[1] = (10 + random.nextInt(25));
            beg[2] = (10 + random.nextInt(25));
            beg[3] = random.nextInt(9);
            beg[4] = random.nextInt(9);
            beg[5] = random.nextInt(9);
            beg[6] = random.nextInt(9);
            beg[7] = random.nextInt(9);
            int sum = 0;
            for (int i = 0; i < 8; i++) {
                sum += beg[i] * numb[i];
            }
            beg[8] = (10 - sum % 10) % 10;
            for (int i = 0; i < 3; i++) {
                this.value += aplha.charAt(beg[i] - 10);
            }
            for (int i = 3; i < 9; i++) {
                this.value += beg[i];
            }
        }
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
