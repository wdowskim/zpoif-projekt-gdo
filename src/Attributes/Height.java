package Attributes;

import java.sql.Date;
import java.util.Map;
import java.util.Random;

public class Height extends Attribute {

    private String value;



    @Override
    public void generate(Map<String, Attribute> attributes) {

        Random random = new Random();
        Sex sex = (Sex) attributes.get("Sex");
        BirthMonth birthMonth = (BirthMonth) attributes.get("BirthMonth");
        BirthYear birthYear = (BirthYear) attributes.get("BirthYear");
        BirthDay birthDay = (BirthDay) attributes.get("BirthDay");
        int d = (int) birthDay.getValue();
        int m = birthMonth.getNumeric();
        int y = Integer.parseInt(String.valueOf(birthYear));
        Date birth_date = new Date(y,m,d);
        Date current_date = new Date(2020,12,31);
        long diff = (current_date.getTime() - birth_date.getTime())/(1000*60*60*24*365) ;
        double df = 2020 - y + 1 - m/12;
        if (sex.isMale()) {
            df = Math.floor((1.9 - 1.3 * Math.exp(-0.21 * df) - 0.1 * Math.exp(0.012 * df)) * 100*(1-random.nextGaussian()/20)) / 100;
        }
        else if (sex.isFemale()){
            df = Math.floor((1.8 - 1.2 * Math.exp(-0.15 * df) - 0.1 * Math.exp(0.009 * df)) * 100*(1-random.nextGaussian()/20)) / 100;
        }
        else {
            df = Math.floor((1.85 - 1.25 * Math.exp(-0.17 * df) - 0.1 * Math.exp(0.01 * df)) * 100*(1-random.nextGaussian()/20)) / 100;
        }
        value = Double.toString(df);



    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public Object getValue() {
        return value;
    }
}
