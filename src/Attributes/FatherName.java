package Attributes;

import Base.Probability;
import Base.ProbabilityFromFile;

import java.io.*;
import java.util.Map;

public class FatherName extends Attribute {
    /*
    Klasa generująca pierwsze imię.
    */

    // value - wartość danego atrybutu
    private String value;

    // maleNames - prawdopodobieństwo z pliku imion mężczyzn
    private ProbabilityFromFile maleNames;

    public FatherName(String maleNamesPath) throws IOException
    {
        this.maleNames = new ProbabilityFromFile(maleNamesPath);
    }

    @Override
    public void generate(Map<String, Attribute> attributes)
    {
        this.value = this.maleNames.generate();
    }

    @Override
    public String toString()
    {
        return this.value;
    }

    @Override
    public String getValue()
    {
        return this.value;
    }
}
